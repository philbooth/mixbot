// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const { assert } = require('chai')
const request = require('request')

module.exports = {
  assertIsRoute,
  assertJoiKeys,
  assertIsJoi,
  initialiseClient
}

function assertIsRoute (object) {
  assert.isObject(object)
  assert.isObject(object.config)
  assert.isObject(object.config.validate)
  assert.isFunction(object.handler)
  assert.lengthOf(object.handler, 2)
}

function assertJoiKeys (parent, keys) {
  assert.isObject(parent)
  assert.lengthOf(Object.keys(parent), keys.length)
  keys.forEach(key => assertIsJoi(parent[key]))
}

function assertIsJoi (property) {
  assert.isObject(property)
  assert.isTrue(property.isJoi)
}

function initialiseClient (options) {
  const client = request.defaults(options)

  return {
    get: requestify(client.get),
    post: requestify(client.post),
    put: requestify(client.put),
    delete: requestify(client.delete),
  }

  function requestify (fn) {
    return (path, payload) => {
      return promisify(
        fn.bind(client, path, payload)
      ).then((response) => {
        if (response.statusCode >= 400) {
          const error = new Error(`${response.body.error}: ${response.body.message}`)
          error.status = response.statusCode
          error.validation = response.body.validation
          throw error
        }

        return response.body
      })
    }
  }
}

function promisify (fn) {
  return new Promise((resolve, reject) => {
    fn((error, result) => {
      if (error) {
        return reject(error)
      }

      resolve(result)
    })
  })
}

