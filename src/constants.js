// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

module.exports = {
  entities: {
    TRACK: 'track',
    ARTIST: 'artist'
  },
  predicates: {
    MIXES_INTO: 'mixes into',
    VERSION_OF: 'is version of',
    SAMPLES: 'samples',
    CONTRIBUTED_TO: 'contributed to'
  },
  versionTypes: {
    TAKE: 'take',
    REMASTER: 'remaster',
    REMIX: 'remix',
    EDIT: 'edit',
    COVER: 'cover',
    LIVE: 'live'
  },
  roles: {
    CONTRIBUTED_TO: 'contributed to',
    WROTE: 'wrote',
    PRODUCED: 'produced',
    PERFORMED: 'performed',
    REMIXED: 'remixed',
    REMASTERED: 'remastered',
    EDITED: 'edited',
    FEATURED_ON: 'featured on'
  }
}

