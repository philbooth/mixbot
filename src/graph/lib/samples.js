// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const db = require('./database')
const assert = require('./assert')
const { predicates } = require('../../constants')

module.exports = initialise

function initialise (master, local, log) {
  return {
    addSample,
    deleteSample,
    getSampled,
    getSampling
  }

  // Add sampled track
  function addSample (sampling, sampled) {
    log.trace({ sampling, sampled }, 'addSample')

    assert.track(sampling)
    assert.track(sampled)

    const triple = {
      subject: sampling,
      predicate: predicates.SAMPLES,
      object: sampled
    }

    return db.get.call(master, triple).then(results => {
      if (results.length === 0) {
        db.put.call(master, triple)
      }
    })
  }

  // Delete sampled track
  function deleteSample (sampling, sampled) {
    log.trace({ sampling, sampled }, 'deleteSample')

    assert.track(sampling)
    assert.track(sampled)

    return db.del.call(master, {
      subject: sampling,
      predicate: predicates.SAMPLES,
      object: sampled
    })
  }

  // Get samples used by a specific track
  function getSampled (sampling) {
    log.trace({ sampling }, 'getSampled')

    assert.track(sampling)

    return db.get.call(master, {
      subject: sampling,
      predicate: predicates.SAMPLES
    })
  }

  // Get tracks that sample a specific track
  function getSampling (sampled) {
    log.trace({ sampled }, 'getSampling')

    assert.track(sampled)

    return db.get.call(master, {
      predicate: predicates.SAMPLES,
      object: sampled
    })
  }
}

