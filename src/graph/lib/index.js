// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const bunyan = require('bunyan')
const config = require('../../../config')
const path = require('path')
const levelup = require('level')
const sublevel = require('level-sublevel')
const levelgraph = require('levelgraph')
const check = require('check-types')
const mixes = require('./mixes')
const versions = require('./versions')
const samples = require('./samples')
const artists = require('./artists')

const databasePath = path.resolve(__dirname, config.get('graph.database.path'))
const database = sublevel(levelup(databasePath))
const master = levelgraph(database.sublevel('master'))

// Eventually, this may have performance implications
const graphs = new Map()

module.exports = initialise

function initialise (id, options) {
  check.assert.nonEmptyString(id, `INPUT ERROR! id, ${id}`)
  check.assert.object(options, `INPUT ERROR! options, ${options}`)

  const log = options.log || bunyan.createLogger({ name: 'graph' })
  log.trace({ databasePath, id, options }, 'initialise')

  if (! graphs.has(id)) {
    const local = levelgraph(database.sublevel(id))

    graphs.set(id, Object.assign(
      {},
      mixes(master, local, log),
      versions(master, local, log),
      samples(master, local, log),
      artists(master, local, log)
    ))
  }

  return graphs.get(id)
}

