// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const check = require('check-types')
const db = require('./database')
const assert = require('./assert')
const { predicates, versionTypes } = require('../../constants')

module.exports = initialise

function initialise (master, local, log) {
  return {
    addVersion,
    deleteVersion,
    getVersions
  }

  // Add alternative version of a track
  function addVersion (original, derived, versionType) {
    log.trace({ original, derived }, 'addVersion')

    versionType = versionType || versionTypes.TAKE

    assert.track(original)
    assert.track(derived)
    assert.versionType(versionType)

    const triple = {
      subject: derived,
      predicate: predicates.VERSION_OF,
      object: original
    }

    return db.get.call(master, triple).then(results => {
      const put = db.put.bind(master, Object.assign({ versionType }, triple))

      if (results.length === 0) {
        return put()
      }

      return db.del.call(master, triple).then(put)
    })
  }

  // Delete alternate version of a track
  function deleteVersion (original, derived) {
    log.trace({ original, derived }, 'deleteVersion')

    assert.track(original)
    assert.track(derived)

    return db.del.call(master, {
      subject: derived,
      predicate: predicates.VERSION_OF,
      object: original
    })
  }

  // Get alternative versions of a track
  function getVersions (track, versionType) {
    log.trace({ track }, 'getVersions')

    assert.track(track)
    check.assigned(versionType) && assert.versionType(versionType)

    let filter, versions

    if (versionType) {
      filter = version => version.versionType === versionType
    } else {
      filter = () => true
    }

    return Promise.all([
      db.get.call(master, {
        subject: track,
        predicate: predicates.VERSION_OF
      }),
      db.get.call(master, {
        predicate: predicates.VERSION_OF,
        object: track
      })
    ]).then(v => {
      versions = v[0].filter(filter).concat(v[1].filter(filter))

      return Promise.all(
        v[0].map(version => db.get.call(master, {
          predicate: predicates.VERSION_OF,
          object: version.object
        }))
      )
    }).then(v => {
      const deduped = {}

      return v.reduce((aggregate, peers) =>
        aggregate.concat(peers.filter(filter))
      , versions).filter(version =>
        ! Array.isArray(version)
      ).filter(version => {
        let target

        if (track === version.subject) {
          target = version.object
        } else {
          target = version.subject
        }

        if (deduped[target]) {
          return false
        }

        return deduped[target] = true
      })
    })
  }
}

