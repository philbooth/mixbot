// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const check = require('check-types')
const db = require('./database')
const assert = require('./assert')
const { predicates } = require('../../constants')

module.exports = initialise

function initialise (master, local, log) {
  return {
    addMix,
    deleteMix,
    getMixesFrom,
    getMixesTo
  }

  // Add mix from one track to another, with optional weight
  function addMix (from, to, weight) {
    log.trace({ from, to, weight }, 'addMix')

    weight = weight || 100

    assert.track(from)
    assert.track(to)
    assert.weight(weight)

    const triple = {
      subject: from,
      predicate: predicates.MIXES_INTO,
      object: to
    }

    return getMasterAndLocal(triple, 1).then((mixes) =>
      Promise.all([
        updateGraph.call(master, mixes.master, mixes.local ? mixes.local.weight : 0),
        updateGraph.call(local, mixes.local)
      ])
    )

    function updateGraph (mix, previousWeight) {
      log.trace({ mix, previousWeight }, 'addMix::updateGraph')

      let exists = true

      if (! mix) {
        exists = false
        mix = {
          subject: triple.subject,
          predicate: triple.predicate,
          object: triple.object
        }
      }

      if (this === master) {
        setAggregateWeight(mix, weight, previousWeight)
      } else {
        setWeight(mix, weight)
      }

      const put = db.put.bind(this, mix)

      if (exists) {
        return db.del.call(this, triple).then(put)
      }

      return put()
    }
  }

  function getMasterAndLocal (triple, limit) {
    log.trace({ triple, limit }, 'getMasterAndLocal')

    return Promise.all([
      db.get.call(master, triple, limit),
      db.get.call(local, triple, limit)
    ]).then(results => {
      log.trace({ results }, 'getMasterAndLocal::then')

      return {
        master: results[0],
        local: results[1]
      }
    })
  }

  function setAggregateWeight (mix, weight, replacesWeight) {
    log.trace({ mix, weight, replacesWeight }, 'setAggregateWeight')

    if (check.positive(mix.aggregateWeight)) {
      assert.property(mix, 'aggregateWeight', 'positive')
      assert.property(mix, 'weightCount', 'positive')
    }
    assert.data([ replacesWeight, 0, 100 ], 'inRange')

    if (mix.aggregateWeight) {
      mix.aggregateWeight = mix.aggregateWeight + weight - replacesWeight
      if (replacesWeight === 0) {
        mix.weightCount += 1
      }
    } else {
      mix.aggregateWeight = weight
      mix.weightCount = 1
    }
  }

  function setWeight (mix, weight) {
    log.trace({ mix, weight }, 'setWeight')

    mix.weight = weight
  }

  // Delete mix from one track to another
  function deleteMix (from, to) {
    log.trace({ from, to }, 'deleteMix')

    assert.track(from)
    assert.track(to)

    const triple = {
      subject: from,
      predicate: predicates.MIXES_INTO,
      object: to
    }

    return getMasterAndLocal(triple, 1).then((mixes) => {
      log.trace({ mixes }, 'deleteMix::then')

      if (mixes.local) {
        assert.property(mixes, 'master', 'object')

        unsetAggregateWeight(mixes.master, mixes.local.weight)

        let promise = db.del.call(master, triple)
        if (mixes.master.aggregateWeight) {
          promise = promise.then(db.put.bind(master, mixes.master))
        }

        return Promise.all([ promise, db.del.call(local, triple) ])
      }
    })
  }

  function unsetAggregateWeight (mix, weight) {
    log.trace({ mix, weight }, 'unsetAggregateWeight')

    assert.property(mix, 'aggregateWeight', 'positive')
    assert.property(mix, 'weightCount', 'positive')

    mix.aggregateWeight -= weight
    mix.weightCount -= 1

    if (check.not.positive(mix.aggregateWeight)) {
      assert.property(mix, 'aggregateWeight', 'zero')
      assert.property(mix, 'weightCount', 'zero')
    }
  }

  // Get mixes out from a preceding track, optionally filtered by weight
  function getMixesFrom (from, weight) {
    log.trace({ from, weight }, 'getMixesFrom')

    // TODO: implement filtering by weight
    weight = weight || 1

    assert.track(from)
    assert.weight(weight)

    // TODO: consider pagination
    return getMasterAndLocal({
      subject: from,
      predicate: predicates.MIXES_INTO
    }, 20)
  }

  // Get mixes in to a following track, optionally filtered by weight
  function getMixesTo (to, weight) {
    log.trace({ to, weight }, 'getMixesTo')

    // TODO: implement filtering by weight
    weight = weight || 1

    assert.track(to)
    assert.weight(weight)

    // TODO: consider pagination
    return getMasterAndLocal({
      predicate: predicates.MIXES_INTO,
      object: to
    }, 20)
  }
}

