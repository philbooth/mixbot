// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const check = require('check-types')
const db = require('./database')
const assert = require('./assert')
const { predicates, roles } = require('../../constants')

module.exports = initialise

function initialise (master, local, log) {
  return {
    addArtist,
    deleteArtist,
    getArtists,
    getTracks
  }

  // Add a contributing artist to a track, with optional role
  function addArtist (track, artist, role) {
    log.trace({ track, artist, role }, 'addArtist')

    role = role || roles.CONTRIBUTED_TO

    assert.track(track)
    assert.artist(artist)
    assert.role(role)

    const triple = {
      subject: artist,
      predicate: predicates.CONTRIBUTED_TO,
      object: track
    }

    return db.get.call(master, triple).then(results => {
      let artistRoles

      if (results.length === 0) {
        artistRoles = [ role ]
      } else if (results[0].roles.indexOf(role) >= 0) {
        artistRoles = results[0].roles
      } else {
        artistRoles = results[0].roles.concat(role)
      }

      const put = db.put.bind(master, Object.assign({ roles: artistRoles }, triple))

      if (results.length === 0) {
        return put()
      }

      return db.del.call(master, triple).then(put)
    })
  }

  // Delete a contributing artist from a track, optionally filtered by their role
  function deleteArtist (track, artist, role) {
    log.trace({ track, artist, role }, 'deleteArtist')

    assert.track(track)
    assert.artist(artist)
    check.assigned(role) && assert.role(role)

    const triple = {
      subject: artist,
      predicate: predicates.CONTRIBUTED_TO,
      object: track
    }

    return db.get.call(master, triple).then(results => {
      if (results.length === 0) {
        return
      }

      const result = results[0]
      const del = db.del.bind(master, triple)

      if (! role || result.roles.length === 1 && result.roles[0] === role) {
        return del()
      }

      const artistRoles = result.roles.filter(r => r !== role)

      return del().then(() =>
        db.put.call(master, Object.assign({ roles: artistRoles }, triple))
      )
    })
  }

  // Get contributing artists for a track, optionally filtered by role
  function getArtists (track, role) {
    log.trace({ track, role }, 'getArtists')

    assert.track(track)
    check.assigned(role) && assert.role(role)

    return db.get.call(master, {
      predicate: predicates.CONTRIBUTED_TO,
      object: track
    }).then(results => {
      if (! role) {
        return results
      }

      return results.filter(result => result.roles.indexOf(role) !== -1)
    })
  }

  // Get tracks an artist has contributed to, optionally filtered by role
  function getTracks (artist, role) {
    log.trace({ artist, role }, 'getTracks')

    assert.artist(artist)
    check.assigned(role) && assert.role(role)

    return db.get.call(master, {
      subject: artist,
      predicate: predicates.CONTRIBUTED_TO
    }).then(results => {
      if (! role) {
        return results
      }

      return results.filter(result => result.roles.indexOf(role) !== -1)
    })
  }
}

