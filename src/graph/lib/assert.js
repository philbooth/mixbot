// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const check = require('check-types')
const { versionTypes, roles } = require('../../constants')

module.exports = { result, property, data, track, weight, versionType, artist, role }

function result (r, triple) {
  Object.keys(triple).forEach(key =>
    check.assert(
      r[key] === triple[key],
      `DATA ERROR! ===, ${key}, ${r[key]}, ${triple[key]}`
    )
  )
}

function property (object, p, predicate) {
  data([ object[p] ], predicate)
}

function data (d, predicate) {
  check.assert[predicate].apply(
    null,
    d.concat(
      d.reduce((message, datum) =>
        `${message}, ${datum}`
      , `DATA ERROR! ${predicate}`)
    )
  )
}

function track (t) {
  check.assert.nonEmptyString(t, `INPUT ERROR! track, ${t}`)
}

function weight (w) {
  check.assert.inRange(w, 1, 100, `INPUT ERROR! weight, ${w}`)
}

function versionType (t) {
  check.assert.includes(Object.values(versionTypes), t, `INPUT ERROR! versionType, ${t}`)
}

function artist (a) {
  check.assert.nonEmptyString(a, `INPUT ERROR! artist, ${a}`)
}

function role (r) {
  check.assert.includes(Object.values(roles), r, `INPUT ERROR! role, ${r}`)
}

