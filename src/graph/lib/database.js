// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const assert = require('./assert')
const check = require('check-types')
const log = require('bunyan').createLogger({ name: 'graph/database' })

module.exports = { get, del, put }

function get (triple, limit) {
  log.trace({ triple, limit }, 'get')

  return callGraph(this, 'get', triple).then(result => {
    log.trace({ result }, 'get::then')

    assert.property({ result }, 'result', 'array')

    if (limit === 1) {
      if (result.length === 0 ) {
        result = null
      } else {
        result = result[0]
        assertResult(result)
      }
    } else {
      if (limit > 0) {
        result = result.slice(0, limit)
      }

      result.forEach(assertResult)
    }

    return result
  })

  function assertResult (result) {
    assert.result(result, triple)
  }
}

function callGraph (graph, operation, triple) {
  return new Promise((resolve, reject) => {
    log.trace({ graph, operation, triple }, 'callGraph')

    graph[operation](triple, handleResult(resolve, reject))
  })
}

function handleResult (resolve, reject) {
  return (error, result) => {
    try {
      check.assert.not.assigned(error, `DB ERROR! ${error}`)
      resolve(result)
    } catch (err) {
      log.error({ error: error || err }, 'DB ERROR!')

      reject(error)
    }
  }
}

function del (triple) {
  log.trace({ triple }, 'del')

  return callGraph(this, 'del', triple)
}

function put (triple) {
  log.trace({ triple }, 'put')

  return callGraph(this, 'put', triple)
}

