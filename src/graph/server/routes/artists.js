// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const graph = require('../../lib')
const joi = require('joi')
const { roles } = require('../../../constants')

const path = '/{graph}/artists'
const hex32 = joi.string().hex().length(64)
const artistRole = joi.string().valid(Object.values(roles))
const trackArtist = joi.object().keys({
  artist: hex32.required(),
  track: hex32.required(),
  roles: joi.array().items(artistRole).required()
})

module.exports = [
  {
    method: 'POST',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          track: hex32.required(),
          artist: hex32.required(),
          role: artistRole.required()
        }
      }
    },
    handler: addArtist
  },
  {
    method: 'DELETE',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          track: hex32.required(),
          artist: hex32.required()
        },
        query: {
          role: artistRole.optional()
        }
      }
    },
    handler: deleteArtist
  },
  {
    method: 'GET',
    path: `${path}/{track}`,
    config: {
      validate: {
        params: {
          graph: hex32.required(),
          track: hex32.required()
        },
        query: {
          role: artistRole.optional()
        }
      },
      response: {
        schema: joi.array().items(trackArtist)
      }
    },
    handler: getArtists
  },
  {
    method: 'GET',
    path: '/{graph}/tracks/{artist}',
    config: {
      validate: {
        params: {
          graph: hex32.required(),
          artist: hex32.required()
        },
        query: {
          role: artistRole.optional()
        }
      },
      response: {
        schema: joi.array().items(trackArtist)
      }
    },
    handler: getTracks
  }
]

function addArtist (request, reply) {
  const graphId = request.params.graph
  const { track, artist, role } = request.payload

  graph(graphId, {}).addArtist(track, artist, role).then(() =>
    reply().code(204)
  ).catch(error => {
    reply(error).code(500)
  })
}

function deleteArtist (request, reply) {
  const graphId = request.params.graph
  const { track, artist } = request.payload
  const role = request.query.role

  graph(graphId, {}).deleteArtist(track, artist, role).then(() =>
    reply().code(204)
  ).catch(error =>
    reply(error).code(500)
  )
}

function getArtists (request, reply) {
  const { graph: graphId, track } = request.params
  const role = request.query.role

  graph(graphId, {}).getArtists(track, role).then(result => {
    reply(marshallResult(result))
  }).catch(error => {
    reply(error).code(500)
  })
}

function marshallResult (result) {
  return result.map(item => {
    return {
      artist: item.subject,
      track: item.object,
      roles: item.roles
    }
  })
}

function getTracks (request, reply) {
  const { graph: graphId, artist } = request.params
  const role = request.query.role

  graph(graphId, {}).getTracks(artist, role).then(result =>
    reply(marshallResult(result))
  ).catch(error =>
    reply(error).code(500)
  )
}

