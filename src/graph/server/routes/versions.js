// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const graph = require('../../lib')
const joi = require('joi')
const { versionTypes: types } = require('../../../constants')

const path = '/{graph}/versions'
const hex32 = joi.string().hex().length(64)
const versionType = joi.string().valid(Object.values(types))
const version = joi.object().keys({
  original: hex32.required(),
  derived: hex32.required(),
  type: versionType.required()
})

module.exports = [
  {
    method: 'POST',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          original: hex32.required(),
          derived: hex32.required(),
          type: versionType.required()
        }
      }
    },
    handler: addVersion
  },
  {
    method: 'DELETE',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          original: hex32.required(),
          derived: hex32.required()
        }
      }
    },
    handler: deleteVersion
  },
  {
    method: 'GET',
    path: `${path}/{track}`,
    config: {
      validate: {
        params: {
          graph: hex32.required(),
          track: hex32.required()
        },
        query: {
          type: versionType.optional()
        }
      },
      response: {
        schema: joi.array().items(version)
      }
    },
    handler: getVersions
  }
]

function addVersion (request, reply) {
  const graphId = request.params.graph
  const { original, derived, type } = request.payload

  graph(graphId, {}).addVersion(original, derived, type).then(() =>
    reply().code(204)
  ).catch(error => {
    reply(error).code(500)
  })
}

function deleteVersion (request, reply) {
  const graphId = request.params.graph
  const { original, derived } = request.payload

  graph(graphId, {}).deleteVersion(original, derived).then(() =>
    reply().code(204)
  ).catch(error =>
    reply(error).code(500)
  )
}

function getVersions (request, reply) {
  const { graph: graphId, track } = request.params
  const type = request.query.type

  graph(graphId, {}).getVersions(track, type).then(result => {
    reply(marshallResult(result))
  }).catch(error => {
    reply(error).code(500)
  })
}

function marshallResult (result) {
  return result.map(item => {
    return {
      derived: item.subject,
      original: item.object,
      type: item.versionType
    }
  })
}

