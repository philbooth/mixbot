// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const graph = require('../../lib')
const joi = require('joi')

const path = '/{graph}/samples'
const hex32 = joi.string().hex().length(64)
const sample = joi.object().keys({
  sampling: hex32.required(),
  sampled: hex32.required()
})

module.exports = [
  {
    method: 'POST',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          sampling: hex32.required(),
          sampled: hex32.required()
        }
      }
    },
    handler: addSample
  },
  {
    method: 'DELETE',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          sampling: hex32.required(),
          sampled: hex32.required()
        }
      }
    },
    handler: deleteSample
  },
  {
    method: 'GET',
    path: '/{graph}/sampled/{sampling}',
    config: {
      validate: {
        params: {
          graph: hex32.required(),
          sampling: hex32.required()
        }
      },
      response: {
        schema: joi.array().items(sample)
      }
    },
    handler: getSampled
  },
  {
    method: 'GET',
    path: '/{graph}/sampling/{sampled}',
    config: {
      validate: {
        params: {
          graph: hex32.required(),
          sampled: hex32.required()
        }
      },
      response: {
        schema: joi.array().items(sample)
      }
    },
    handler: getSampling
  }
]

function addSample (request, reply) {
  const graphId = request.params.graph
  const { sampling, sampled } = request.payload

  graph(graphId, {}).addSample(sampling, sampled).then(() =>
    reply().code(204)
  ).catch(error =>
    reply(error).code(500)
  )
}

function deleteSample (request, reply) {
  const graphId = request.params.graph
  const { sampling, sampled } = request.payload

  graph(graphId, {}).deleteSample(sampling, sampled).then(() =>
    reply().code(204)
  ).catch(error =>
    reply(error).code(500)
  )
}

function getSampled (request, reply) {
  const { graph: graphId, sampling } = request.params

  graph(graphId, {}).getSampled(sampling).then(result => {
    reply(marshallResult(result))
  }).catch(error => {
    reply(error).code(500)
  })
}

function marshallResult (result) {
  return result.map(item => {
    return {
      sampling: item.subject,
      sampled: item.object
    }
  })
}

function getSampling (request, reply) {
  const { graph: graphId, sampled } = request.params

  graph(graphId, {}).getSampling(sampled).then(result =>
    reply(marshallResult(result))
  ).catch(error =>
    reply(error).code(500)
  )
}

