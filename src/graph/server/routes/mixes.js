// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const graph = require('../../lib')
const joi = require('joi')

const path = '/{graph}/mixes'
const hex32 = joi.string().hex().length(64)
const mixWeight = joi.number().min(1).max(100)
const mix = joi.object().keys({
  from: hex32.required(),
  to: hex32.required(),
  weight: mixWeight.required()
})

module.exports = [
  {
    method: 'POST',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          from: hex32.required(),
          to: hex32.required(),
          weight: mixWeight.required()
        }
      }
    },
    handler: addMix
  },
  {
    method: 'DELETE',
    path,
    config: {
      validate: {
        params: {
          graph: hex32.required()
        },
        payload: {
          from: hex32.required(),
          to: hex32.required()
        }
      }
    },
    handler: deleteMix
  },
  {
    method: 'GET',
    path: `${path}/from/{from}`,
    config: {
      validate: {
        params: {
          graph: hex32.required(),
          from: hex32.required()
        },
        query: {
          weight: mixWeight.optional()
        }
      },
      response: {
        schema: {
          master: joi.array().items(mix),
          local: joi.array().items(mix)
        }
      }
    },
    handler: getMixesFrom
  },
  {
    method: 'GET',
    path: `${path}/to/{to}`,
    config: {
      validate: {
        params: {
          graph: hex32.required(),
          to: hex32.required()
        },
        query: {
          weight: mixWeight.optional()
        }
      },
      response: {
        schema: {
          master: joi.array().items(mix),
          local: joi.array().items(mix)
        }
      }
    },
    handler: getMixesTo
  }
]

function addMix (request, reply) {
  const graphId = request.params.graph
  const { from, to, weight } = request.payload

  graph(graphId, {}).addMix(from, to, weight).then(() =>
    reply().code(204)
  ).catch(error =>
    reply(error).code(500)
  )
}

function deleteMix (request, reply) {
  const graphId = request.params.graph
  const { from, to } = request.payload

  graph(graphId, {}).deleteMix(from, to).then(() =>
    reply().code(204)
  ).catch(error =>
    reply(error).code(500)
  )
}

function getMixesFrom (request, reply) {
  const { graph: graphId, from } = request.params
  const weight = request.query.weight

  graph(graphId, {}).getMixesFrom(from, weight).then(result => {
    reply(marshallResult(result))
  }).catch(error => {
    reply(error).code(500)
  })
}

function marshallResult (result) {
  return {
    master: result.master.map(item => {
      return {
        from: item.subject,
        to: item.object,
        weight: item.aggregateWeight / item.weightCount
      }
    }),
    local: result.local.map(item => {
      return {
        from: item.subject,
        to: item.object,
        weight: item.weight
      }
    })
  }
}

function getMixesTo (request, reply) {
  const { graph: graphId, to } = request.params
  const weight = request.query.weight

  graph(graphId, {}).getMixesTo(to, weight).then(result =>
    reply(marshallResult(result))
  ).catch(error =>
    reply(error).code(500)
  )
}

