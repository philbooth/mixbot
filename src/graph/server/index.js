// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const config = require('../../../config').get('graph.server')
const bunyan = require('bunyan')
const hapi = require('hapi')
const check = require('check-types')
const requireDirectory = require('require-directory')

module.exports = initialise

function initialise (options) {
  return new Promise((resolve, reject) => {
    check.assert.object(options, `INPUT ERROR! options, ${options}`)

    const log = options.log || bunyan.createLogger({ name: 'server' })

    log.info('starting')

    const server = new hapi.Server()
    server.connection(config)

    const routes = requireDirectory(module, './routes')
    server.route(flatten(Object.values(routes)))

    server.on('start', () =>
      resolve({ stop: () => stop(server, log) })
    )

    server.start(handleEvent('started', reject, log))
  })
}

function flatten (array) {
  return Array.prototype.concat.apply([], array)
}

function stop (server, log) {
  return new Promise((resolve, reject) => {
    server.on('stop', resolve)

    server.stop(handleEvent('stopped', reject, log))
  })
}

function handleEvent (event, reject, log) {
  return error => {
    if (error) {
      log.error({ error }, 'SERVER ERROR!')
      return reject(error)
    }

    log.info(config, event)
  }
}

