// Copyright © 2017 Phil Booth.
//
// This file is part of mixbot-identity.
//
// mixbot-identity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot-identity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot-identity. If not, see <http://www.gnu.org/licenses/>.

/* eslint no-process-exit: 0 */

'use strict'

const bunyan = require('bunyan')

const SIGNALS = new Map([
  [ 'SIGHUP', 1 ],
  [ 'SIGINT', 2 ],
  [ 'SIGTERM', 15 ]
])

require('./server')({}).then(server => {
  const log = bunyan.createLogger({ name: 'process' })

  SIGNALS.forEach((value, name) => {
    process.on(name, () => {
      log.info({ signal: name }, 'terminating')
      server.stop().then(() => process.exit(128 + value))
    })
  })
})

