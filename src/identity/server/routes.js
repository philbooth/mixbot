// Copyright © 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const joi = require('joi')

const is = {
  id: joi.string().hex().length(64),
  pattern: joi.string().strict().trim().min(2)
}
is.result = joi.object().keys({
  id: is.id,
  name: is.pattern.required()
})

module.exports = require('../lib')({})
  .then(identity => {
    return [
      {
        method: 'GET',
        path: '/artists/{pattern}',
        config: {
          validate: {
            params: {
              pattern: is.pattern.required()
            }
          },
          response: {
            schema: joi.array().items(is.result).required()
          }
        },
        handler: getArtists
      },
      {
        method: 'PUT',
        path: '/artist',
        config: {
          validate: {
            payload: {
              name: is.pattern.required()
            }
          }
        },
        handler: putArtist
      },
      {
        method: 'GET',
        path: '/tracks/{pattern}',
        config: {
          validate: {
            params: {
              pattern: is.pattern.required()
            }
          },
          response: {
            schema: joi.array().items(is.result).required()
          }
        },
        handler: getTracks
      },
      {
        method: 'PUT',
        path: '/track',
        config: {
          validate: {
            payload: {
              name: is.pattern.required()
            }
          }
        },
        handler: putTrack
      }
    ]

    function getArtists (request, reply) {
      const { pattern } = request.params

      try {
        reply(identity.getArtists(pattern))
      } catch (error) {
        reply(error).code(500)
      }
    }

    function putArtist (request, reply) {
      const { name } = request.payload

      try {
        reply(identity.setArtist(name)).code(204)
      } catch (error) {
        reply(error).code(500)
      }
    }

    function getTracks (request, reply) {
      const { pattern } = request.params

      try {
        reply(identity.getTracks(pattern))
      } catch (error) {
        reply(error).code(500)
      }
    }

    function putTrack (request, reply) {
      const { name } = request.payload

      try {
        reply(identity.setTrack(name)).code(204)
      } catch (error) {
        reply(error).code(500)
      }
    }
  })

