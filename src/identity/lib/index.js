// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const bunyan = require('bunyan')
const check = require('check-types')
const config = require('../../../config')
const crypto = require('crypto')
const levelup = require('level')
const path = require('path')
const sublevel = require('level-sublevel')
const surch = require('surch')
const util = require('util')

const databasePath = path.resolve(__dirname, config.get('identity.database.path'))
const database = sublevel(levelup(databasePath))
const artists = initialiseIndex('artists')
const tracks = initialiseIndex('tracks')

crypto.randomBytesp = util.promisify(crypto.randomBytes)

module.exports = initialise

function initialiseIndex (name) {
  const db = database.sublevel(name)
  db.putp = util.promisify(db.put)
  db.getp = util.promisify(db.get)
  db.delp = util.promisify(db.del)

  const baseName = name.substr(0, 1).toUpperCase().concat(name.substr(1, name.length - 2))

  return {
    db,
    index: surch.create('name', {
      idKey: 'id',
      minLength: 2
    }),
    names: {
      get: `get${baseName}s`,
      set: `set${baseName}`,
      del: `del${baseName}`
    }
  }
}

function initialise (options) {
  check.assert.object(options, `INPUT ERROR! options, ${options}`)

  const log = options.log || bunyan.createLogger({ name: 'identity' })
  log.trace({ databasePath }, 'initialise')

  return Promise.all([ sync(artists), sync(tracks) ])
    .then(() => ({
      getArtists: get.bind(artists),
      setArtist: set.bind(artists),
      deleteArtist: del.bind(artists),
      getTracks: get.bind(tracks),
      setTrack: set.bind(tracks),
      deleteTrack: del.bind(tracks)
    }))

  function sync (collection) {
    return new Promise((resolve, reject) => {
      log.trace({ names: collection.names }, 'initialise::sync')

      collection.db.createValueStream()
        .on('data', data => {
          log.trace({ data }, 'initialise::sync::data')

          collection.index.add(data)
        })
        .on('error', error => {
          log.trace({ error }, 'initialise::sync::error')

          reject(error)
        })
        .on('end', () => {
          log.trace('initialise::sync::end')

          resolve()
        })
    })
  }

  function get (pattern) {
    log.trace({ pattern }, this.names.get)

    return this.index.search(pattern)
      .map(result => ({ id: result.id, name: result.match }))
  }

  function set (name) {
    log.trace({ name }, this.names.set)

    let record

    return newId.call(this)
      .then(id => {
        record = { id, name }
        return this.db.putp(id, record)
      })
      .then(() => this.index.add(record))
  }

  function newId () {
    log.trace('newId')

    let id

    return crypto.randomBytesp(32)
      .then(result => {
        id = result.toString('hex')
        return this.db.getp(id)
      })
      .then(() => newId())
      .catch(error => {
        if (error.notFound) {
          return id
        }

        log.error(error)
        return newId()
      })
  }

  function del (id) {
    log.trace({ id }, this.names.del)

    return this.db.delp(id)
      .then(() => this.index.delete(id))
  }
}

