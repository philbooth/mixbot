// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert
const config = require('../../../../config').get('graph.server')
const crypto = require('crypto')
const helpers = require('../../../../src/test-helpers')

const modulePath = '../../../../src/graph/server/routes/artists'
const client = helpers.initialiseClient({
  baseUrl: `http://127.0.0.1:${config.port}/`,
  json: true,
  timeout: 3000
})

suite('server/routes/artists:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  suite('require:', () => {
    let artists

    suiteSetup(() => artists = require(modulePath))

    test('require returns array', () =>
      assert.isArray(artists)
    )

    test('array has four routes', () => {
      assert.lengthOf(artists, 4)

      helpers.assertIsRoute(artists[0])
      helpers.assertIsRoute(artists[1])
      helpers.assertIsRoute(artists[2])
      helpers.assertIsRoute(artists[3])
    })

    test('first route is correct', () => {
      assert.equal(artists[0].method, 'POST')
      assert.equal(artists[0].path, '/{graph}/artists')

      assert.lengthOf(Object.keys(artists[0].config.validate), 2)
      helpers.assertJoiKeys(artists[0].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(artists[0].config.validate.payload, [ 'track', 'artist', 'role' ])

      assert.isUndefined(artists[0].config.response)
    })

    test('second route is correct', () => {
      assert.equal(artists[1].method, 'DELETE')
      assert.equal(artists[1].path, '/{graph}/artists')

      assert.lengthOf(Object.keys(artists[1].config.validate), 3)
      helpers.assertJoiKeys(artists[1].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(artists[1].config.validate.payload, [ 'track', 'artist' ])
      helpers.assertJoiKeys(artists[2].config.validate.query, [ 'role' ])

      assert.isUndefined(artists[1].config.response)
    })

    test('third route is correct', () => {
      assert.equal(artists[2].method, 'GET')
      assert.equal(artists[2].path, '/{graph}/artists/{track}')

      assert.lengthOf(Object.keys(artists[2].config.validate), 2)
      helpers.assertJoiKeys(artists[2].config.validate.params, [ 'graph', 'track' ])
      helpers.assertJoiKeys(artists[2].config.validate.query, [ 'role' ])

      assert.isObject(artists[2].config.response)
      helpers.assertIsJoi(artists[2].config.response.schema)
    })

    test('fourth route is correct', () => {
      assert.equal(artists[3].method, 'GET')
      assert.equal(artists[3].path, '/{graph}/tracks/{artist}')

      assert.lengthOf(Object.keys(artists[3].config.validate), 2)
      helpers.assertJoiKeys(artists[3].config.validate.params, [ 'graph', 'artist' ])
      helpers.assertJoiKeys(artists[3].config.validate.query, [ 'role' ])

      assert.isObject(artists[3].config.response)
      helpers.assertIsJoi(artists[3].config.response.schema)
    })

    suite('start server:', () => {
      let server, graphId

      suiteSetup(() => {
        graphId = crypto.randomBytes(32).toString('hex')
        return require('../../../../src/graph/server')({})
          .then(s => server = s)
      })

      suiteTeardown(() => server.stop())

      suite('add an artist:', () => {
        let track, artist

        setup(() => {
          track = crypto.randomBytes(32).toString('hex')
          artist = crypto.randomBytes(32).toString('hex')

          return client.post({
            url: `/${graphId}/artists`,
            body: { track, artist, role: 'wrote' }
          })
        })

        suite('get artists with match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/artists/${track}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { track, artist, roles: [ 'wrote' ] } ])
          )
        })

        suite('get artists without match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/artists/${artist}?role=wrote`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('get artists with matching role filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/artists/${track}?role=wrote`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { track, artist, roles: [ 'wrote' ] } ])
          )
        })

        suite('get artists with non-matching role filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/artists/${track}?role=produced`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('attempt to get artists with invalid graphId:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}f/artists/${track}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to get artists with invalid track:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/artists/${new Array(65).join('g')}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "track" fails because ["track" must only contain hexadecimal characters]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'track' ]
            })
          })
        })

        suite('attempt to get artists with invalid role:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/artists/${track}?role=wroted`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "role" fails because ["role" must be one of [contributed to, wrote, produced, performed, remixed, remastered, edited, featured on]]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'query',
              keys: [ 'role' ]
            })
          })
        })

        suite('get tracks with match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/tracks/${artist}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { track, artist, roles: [ 'wrote' ] } ])
          )
        })

        suite('get tracks without match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/tracks/${track}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('get tracks with matching role filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/tracks/${artist}?role=wrote`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { track, artist, roles: [ 'wrote' ] } ])
          )
        })

        suite('get tracks with non-matching role filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/tracks/${artist}?role=performed`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('attempt to get tracks with invalid graphId:', () => {
          let result, error

          setup(() =>
            client.get(`/${new Array(65).join('x')}/tracks/${artist}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" must only contain hexadecimal characters]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to get tracks with invalid artist:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/tracks/${artist.substr(0, 63)}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "artist" fails because ["artist" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'artist' ]
            })
          })
        })

        suite('attempt to get tracks with invalid role:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/tracks/${artist}?role=performer`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "role" fails because ["role" must be one of [contributed to, wrote, produced, performed, remixed, remastered, edited, featured on]]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'query',
              keys: [ 'role' ]
            })
          })
        })

        suite('remove an artist:', () => {
          setup(() =>
            client.delete({
              url: `/${graphId}/artists?role=wrote`,
              body: { track, artist }
            })
          )

          suite('get artists:', () => {
            let result

            setup(() =>
              client.get(`/${graphId}/artists/${track}`)
                .then(r => result = r)
            )

            test('result was correct', () =>
              assert.deepEqual(result, [])
            )
          })

          suite('get tracks:', () => {
            let result

            setup(() =>
              client.get(`/${graphId}/tracks/${artist}`)
                .then(r => result = r)
            )

            test('result was correct', () =>
              assert.deepEqual(result, [])
            )
          })
        })

        suite('attempt to remove an artist with with invalid graphId:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}0/artists`,
              body: {
                track: crypto.randomBytes(32).toString('hex'),
                artist: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to remove an artist with with invalid track:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/artists`,
              body: {
                track: crypto.randomBytes(32).toString('hex').substr(0, 63),
                artist: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "track" fails because ["track" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'track' ]
            })
          })
        })

        suite('attempt to remove an artist with with invalid artist:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/artists`,
              body: {
                track: crypto.randomBytes(32).toString('hex'),
                artist: `0${crypto.randomBytes(32).toString('hex')}`
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "artist" fails because ["artist" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'artist' ]
            })
          })
        })
      })

      suite('attempt to add an artist with with invalid graphId:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId.substr(0, 63)}/artists`,
            body: {
              track: crypto.randomBytes(32).toString('hex'),
              artist: crypto.randomBytes(32).toString('hex'),
              role: 'wrote'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.instanceOf(error, Error)
          assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'params',
            keys: [ 'graph' ]
          })
        })
      })

      suite('attempt to add an artist with with invalid track:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/artists`,
            body: {
              track: `${crypto.randomBytes(32).toString('hex')}f`,
              artist: crypto.randomBytes(32).toString('hex'),
              role: 'wrote'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "track" fails because ["track" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'track' ]
          })
        })
      })

      suite('attempt to add an artist with with invalid artist:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/artists`,
            body: {
              track: crypto.randomBytes(32).toString('hex'),
              artist: crypto.randomBytes(32).toString('hex').substr(0, 63),
              role: 'wrote'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "artist" fails because ["artist" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'artist' ]
          })
        })
      })

      suite('attempt to add an artist with with invalid role:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/artists`,
            body: {
              track: crypto.randomBytes(32).toString('hex'),
              artist: crypto.randomBytes(32).toString('hex'),
              role: 'wibble'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "role" fails because ["role" must be one of [contributed to, wrote, produced, performed, remixed, remastered, edited, featured on]]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'role' ]
          })
        })
      })
    })
  })
})

