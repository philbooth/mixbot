// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert
const config = require('../../../../config').get('graph.server')
const crypto = require('crypto')
const helpers = require('../../../../src/test-helpers')

const modulePath = '../../../../src/graph/server/routes/mixes'
const client = helpers.initialiseClient({
  baseUrl: `http://127.0.0.1:${config.port}/`,
  json: true,
  timeout: 3000
})

suite('server/routes/mixes:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  suite('require:', () => {
    let mixes

    suiteSetup(() => mixes = require(modulePath))

    test('require returns array', () =>
      assert.isArray(mixes)
    )

    test('array has four routes', () => {
      assert.lengthOf(mixes, 4)

      helpers.assertIsRoute(mixes[0])
      helpers.assertIsRoute(mixes[1])
      helpers.assertIsRoute(mixes[2])
      helpers.assertIsRoute(mixes[3])
    })

    test('first route is correct', () => {
      assert.equal(mixes[0].method, 'POST')
      assert.equal(mixes[0].path, '/{graph}/mixes')

      assert.lengthOf(Object.keys(mixes[0].config.validate), 2)
      helpers.assertJoiKeys(mixes[0].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(mixes[0].config.validate.payload, [ 'from', 'to', 'weight' ])

      assert.isUndefined(mixes[0].config.response)
    })

    test('second route is correct', () => {
      assert.equal(mixes[1].method, 'DELETE')
      assert.equal(mixes[1].path, '/{graph}/mixes')

      assert.lengthOf(Object.keys(mixes[1].config.validate), 2)
      helpers.assertJoiKeys(mixes[1].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(mixes[1].config.validate.payload, [ 'from', 'to' ])

      assert.isUndefined(mixes[1].config.response)
    })

    test('third route is correct', () => {
      assert.equal(mixes[2].method, 'GET')
      assert.equal(mixes[2].path, '/{graph}/mixes/from/{from}')

      assert.lengthOf(Object.keys(mixes[2].config.validate), 2)
      helpers.assertJoiKeys(mixes[2].config.validate.params, [ 'graph', 'from' ])
      helpers.assertJoiKeys(mixes[2].config.validate.query, [ 'weight' ])

      assert.isObject(mixes[2].config.response)
      helpers.assertJoiKeys(mixes[2].config.response.schema, [ 'master', 'local' ])
    })

    test('fourth route is correct', () => {
      assert.equal(mixes[3].method, 'GET')
      assert.equal(mixes[3].path, '/{graph}/mixes/to/{to}')

      assert.lengthOf(Object.keys(mixes[3].config.validate), 2)
      helpers.assertJoiKeys(mixes[3].config.validate.params, [ 'graph', 'to' ])
      helpers.assertJoiKeys(mixes[3].config.validate.query, [ 'weight' ])

      assert.isObject(mixes[3].config.response)
      helpers.assertJoiKeys(mixes[3].config.response.schema, [ 'master', 'local' ])
    })

    suite('start server:', () => {
      let server, graphId

      suiteSetup(() => {
        graphId = crypto.randomBytes(32).toString('hex')
        return require('../../../../src/graph/server')({})
          .then(s => server = s)
      })

      suiteTeardown(() => server.stop())

      suite('add a mix:', () => {
        let from, to

        setup(() => {
          from = crypto.randomBytes(32).toString('hex')
          to = crypto.randomBytes(32).toString('hex')

          return client.post({
            url: `/${graphId}/mixes`,
            body: { from, to, weight: 77 }
          })
        })

        suite('get mixes from with match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/from/${from}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [ { from, to, weight: 77 } ],
              local: [ { from, to, weight: 77 } ]
            })
          )
        })

        suite('get mixes from without match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/from/${to}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [],
              local: []
            })
          )
        })

/*        suite('get mixes from with matching weight filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/from/${from}?weight=77`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [ { from, to, weight: 77 } ],
              local: [ { from, to, weight: 77 } ]
            })
          )
        })

        suite('get mixes from with non-matching weight filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/from/${from}?weight=78`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [],
              local: []
            })
          )
        })*/

        suite('attempt to get mixes from with invalid graphId:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}f/mixes/from/${from}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to get mixes from with invalid from:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/mixes/from/${new Array(65).join('g')}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "from" fails because ["from" must only contain hexadecimal characters]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'from' ]
            })
          })
        })

        suite('attempt to get mixes from with invalid weight:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/mixes/from/${from}?weight=101`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "weight" fails because ["weight" must be less than or equal to 100]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'query',
              keys: [ 'weight' ]
            })
          })
        })

        suite('get mixes to with match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/to/${to}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [ { from, to, weight: 77 } ],
              local: [ { from, to, weight: 77 } ]
            })
          )
        })

        suite('get mixes to without match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/to/${from}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [],
              local: []
            })
          )
        })

/*        suite('get mixes to with matching weight filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/to/${to}?weight=76`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [ { from, to, weight: 77 } ],
              local: [ { from, to, weight: 77 } ]
            })
          )
        })

        suite('get mixes to with non-matching weight filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/mixes/to/${to}?weight=78`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, {
              master: [],
              local: []
            })
          )
        })*/

        suite('attempt to get mixes to with invalid graphId:', () => {
          let result, error

          setup(() =>
            client.get(`/${new Array(65).join('x')}/mixes/to/${to}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" must only contain hexadecimal characters]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to get mixes to with invalid to:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/mixes/to/${to.substr(0, 63)}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "to" fails because ["to" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'to' ]
            })
          })
        })

        suite('attempt to get mixes to with invalid weight:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/mixes/to/${to}?weight=0`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "weight" fails because ["weight" must be larger than or equal to 1]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'query',
              keys: [ 'weight' ]
            })
          })
        })

        suite('remove a mix:', () => {
          setup(() =>
            client.delete({
              url: `/${graphId}/mixes`,
              body: { from, to }
            })
          )

          suite('get mixes from:', () => {
            let result

            setup(() =>
              client.get(`/${graphId}/mixes/from/${from}`)
                .then(r => result = r)
            )

            test('result was correct', () =>
              assert.deepEqual(result, {
                master: [],
                local: []
              })
            )
          })

          suite('get mixes to:', () => {
            let result

            setup(() =>
              client.get(`/${graphId}/mixes/to/${to}`)
                .then(r => result = r)
            )

            test('result was correct', () =>
              assert.deepEqual(result, {
                master: [],
                local: []
              })
            )
          })
        })

        suite('attempt to remove a mix with with invalid graphId:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}0/mixes`,
              body: {
                from: crypto.randomBytes(32).toString('hex'),
                to: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to remove a mix with with invalid from:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/mixes`,
              body: {
                from: crypto.randomBytes(32).toString('hex').substr(0, 63),
                to: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "from" fails because ["from" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'from' ]
            })
          })
        })

        suite('attempt to remove a mix with with invalid to:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/mixes`,
              body: {
                from: crypto.randomBytes(32).toString('hex'),
                to: `0${crypto.randomBytes(32).toString('hex')}`
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "to" fails because ["to" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'to' ]
            })
          })
        })
      })

      suite('attempt to add a mix with with invalid graphId:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId.substr(0, 63)}/mixes`,
            body: {
              from: crypto.randomBytes(32).toString('hex'),
              to: crypto.randomBytes(32).toString('hex'),
              weight: 77
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.instanceOf(error, Error)
          assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'params',
            keys: [ 'graph' ]
          })
        })
      })

      suite('attempt to add a mix with with invalid from:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/mixes`,
            body: {
              from: `${crypto.randomBytes(32).toString('hex')}f`,
              to: crypto.randomBytes(32).toString('hex'),
              weight: 77
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "from" fails because ["from" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'from' ]
          })
        })
      })

      suite('attempt to add a mix with with invalid to:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/mixes`,
            body: {
              from: crypto.randomBytes(32).toString('hex'),
              to: crypto.randomBytes(32).toString('hex').substr(0, 63),
              weight: 77
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "to" fails because ["to" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'to' ]
          })
        })
      })

      suite('attempt to add a mix with with invalid weight:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/mixes`,
            body: {
              from: crypto.randomBytes(32).toString('hex'),
              to: crypto.randomBytes(32).toString('hex'),
              weight: 101
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "weight" fails because ["weight" must be less than or equal to 100]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'weight' ]
          })
        })
      })
    })
  })
})

