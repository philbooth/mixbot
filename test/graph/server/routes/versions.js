// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert
const config = require('../../../../config').get('graph.server')
const crypto = require('crypto')
const helpers = require('../../../../src/test-helpers')

const modulePath = '../../../../src/graph/server/routes/versions'
const client = helpers.initialiseClient({
  baseUrl: `http://127.0.0.1:${config.port}/`,
  json: true,
  timeout: 3000
})

suite('server/routes/versions:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  suite('require:', () => {
    let versions

    suiteSetup(() => versions = require(modulePath))

    test('require returns array', () =>
      assert.isArray(versions)
    )

    test('array has four routes', () => {
      assert.lengthOf(versions, 3)

      helpers.assertIsRoute(versions[0])
      helpers.assertIsRoute(versions[1])
      helpers.assertIsRoute(versions[2])
    })

    test('first route is correct', () => {
      assert.equal(versions[0].method, 'POST')
      assert.equal(versions[0].path, '/{graph}/versions')

      assert.lengthOf(Object.keys(versions[0].config.validate), 2)
      helpers.assertJoiKeys(versions[0].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(versions[0].config.validate.payload, [ 'original', 'derived', 'type' ])

      assert.isUndefined(versions[0].config.response)
    })

    test('second route is correct', () => {
      assert.equal(versions[1].method, 'DELETE')
      assert.equal(versions[1].path, '/{graph}/versions')

      assert.lengthOf(Object.keys(versions[1].config.validate), 2)
      helpers.assertJoiKeys(versions[1].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(versions[1].config.validate.payload, [ 'original', 'derived' ])

      assert.isUndefined(versions[1].config.response)
    })

    test('third route is correct', () => {
      assert.equal(versions[2].method, 'GET')
      assert.equal(versions[2].path, '/{graph}/versions/{track}')

      assert.lengthOf(Object.keys(versions[2].config.validate), 2)
      helpers.assertJoiKeys(versions[2].config.validate.params, [ 'graph', 'track' ])
      helpers.assertJoiKeys(versions[2].config.validate.query, [ 'type' ])

      assert.isObject(versions[2].config.response)
      helpers.assertIsJoi(versions[2].config.response.schema)
    })

    suite('start server:', () => {
      let server, graphId

      suiteSetup(() => {
        graphId = crypto.randomBytes(32).toString('hex')
        return require('../../../../src/graph/server')({})
          .then(s => server = s)
      })

      suiteTeardown(() => server.stop())

      suite('add a version:', () => {
        let original, derived

        setup(() => {
          original = crypto.randomBytes(32).toString('hex')
          derived = crypto.randomBytes(32).toString('hex')

          return client.post({
            url: `/${graphId}/versions`,
            body: { original, derived, type: 'remix' }
          })
        })

        suite('get versions with original match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/versions/${original}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { original, derived, type: 'remix' } ])
          )
        })

        suite('get versions with derived match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/versions/${derived}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { original, derived, type: 'remix' } ])
          )
        })

        suite('get versions without match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/versions/${crypto.randomBytes(32).toString('hex')}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('get versions with matching type filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/versions/${original}?type=remix`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { original, derived, type: 'remix' } ])
          )
        })

        suite('get versions with non-matching type filter:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/versions/${original}?type=edit`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('attempt to get versions with invalid graphId:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}f/versions/${original}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to get versions with invalid track:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/versions/${new Array(65).join('g')}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "track" fails because ["track" must only contain hexadecimal characters]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'track' ]
            })
          })
        })

        suite('attempt to get versions with invalid type:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/versions/${original}?type=copy`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "type" fails because ["type" must be one of [take, remaster, remix, edit, cover, live]]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'query',
              keys: [ 'type' ]
            })
          })
        })

        suite('remove a version:', () => {
          setup(() =>
            client.delete({
              url: `/${graphId}/versions`,
              body: { original, derived }
            })
          )

          suite('get versions:', () => {
            let result

            setup(() =>
              client.get(`/${graphId}/versions/${original}`)
                .then(r => result = r)
            )

            test('result was correct', () =>
              assert.deepEqual(result, [])
            )
          })
        })

        suite('attempt to remove a version with with invalid graphId:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}0/versions`,
              body: {
                track: crypto.randomBytes(32).toString('hex'),
                artist: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to remove a version with with invalid original:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/versions`,
              body: {
                original: crypto.randomBytes(32).toString('hex').substr(0, 63),
                derived: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "original" fails because ["original" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'original' ]
            })
          })
        })

        suite('attempt to remove a version with with invalid derived:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/versions`,
              body: {
                original: crypto.randomBytes(32).toString('hex'),
                derived: `0${crypto.randomBytes(32).toString('hex')}`
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "derived" fails because ["derived" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'derived' ]
            })
          })
        })
      })

      suite('attempt to add a version with with invalid graphId:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId.substr(0, 63)}/versions`,
            body: {
              original: crypto.randomBytes(32).toString('hex'),
              derived: crypto.randomBytes(32).toString('hex'),
              type: 'remix'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.instanceOf(error, Error)
          assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'params',
            keys: [ 'graph' ]
          })
        })
      })

      suite('attempt to add a version with with invalid original:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/versions`,
            body: {
              original: `${crypto.randomBytes(32).toString('hex')}f`,
              derived: crypto.randomBytes(32).toString('hex'),
              type: 'remix'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "original" fails because ["original" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'original' ]
          })
        })
      })

      suite('attempt to add a version with with invalid derived:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/versions`,
            body: {
              original: crypto.randomBytes(32).toString('hex'),
              derived: crypto.randomBytes(32).toString('hex').substr(0, 63),
              type: 'remix'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "derived" fails because ["derived" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'derived' ]
          })
        })
      })

      suite('attempt to add a version with with invalid type:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/versions`,
            body: {
              original: crypto.randomBytes(32).toString('hex'),
              derived: crypto.randomBytes(32).toString('hex'),
              type: 'wibble'
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "type" fails because ["type" must be one of [take, remaster, remix, edit, cover, live]]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'type' ]
          })
        })
      })
    })
  })
})

