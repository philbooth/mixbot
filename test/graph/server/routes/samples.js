// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert
const config = require('../../../../config').get('graph.server')
const crypto = require('crypto')
const helpers = require('../../../../src/test-helpers')

const modulePath = '../../../../src/graph/server/routes/samples'
const client = helpers.initialiseClient({
  baseUrl: `http://127.0.0.1:${config.port}/`,
  json: true,
  timeout: 3000
})

suite('server/routes/samples:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  suite('require:', () => {
    let samples

    suiteSetup(() => samples = require(modulePath))

    test('require returns array', () =>
      assert.isArray(samples)
    )

    test('array has four routes', () => {
      assert.lengthOf(samples, 4)

      helpers.assertIsRoute(samples[0])
      helpers.assertIsRoute(samples[1])
      helpers.assertIsRoute(samples[2])
      helpers.assertIsRoute(samples[3])
    })

    test('first route is correct', () => {
      assert.equal(samples[0].method, 'POST')
      assert.equal(samples[0].path, '/{graph}/samples')

      assert.lengthOf(Object.keys(samples[0].config.validate), 2)
      helpers.assertJoiKeys(samples[0].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(samples[0].config.validate.payload, [ 'sampling', 'sampled' ])

      assert.isUndefined(samples[0].config.response)
    })

    test('second route is correct', () => {
      assert.equal(samples[1].method, 'DELETE')
      assert.equal(samples[1].path, '/{graph}/samples')

      assert.lengthOf(Object.keys(samples[1].config.validate), 2)
      helpers.assertJoiKeys(samples[1].config.validate.params, [ 'graph' ])
      helpers.assertJoiKeys(samples[1].config.validate.payload, [ 'sampling', 'sampled' ])

      assert.isUndefined(samples[1].config.response)
    })

    test('third route is correct', () => {
      assert.equal(samples[2].method, 'GET')
      assert.equal(samples[2].path, '/{graph}/sampled/{sampling}')

      assert.lengthOf(Object.keys(samples[2].config.validate), 1)
      helpers.assertJoiKeys(samples[2].config.validate.params, [ 'graph', 'sampling' ])

      assert.isObject(samples[2].config.response)
      helpers.assertIsJoi(samples[2].config.response.schema)
    })

    test('fourth route is correct', () => {
      assert.equal(samples[3].method, 'GET')
      assert.equal(samples[3].path, '/{graph}/sampling/{sampled}')

      assert.lengthOf(Object.keys(samples[3].config.validate), 1)
      helpers.assertJoiKeys(samples[3].config.validate.params, [ 'graph', 'sampled' ])

      assert.isObject(samples[3].config.response)
      helpers.assertIsJoi(samples[3].config.response.schema)
    })

    suite('start server:', () => {
      let server, graphId

      suiteSetup(() => {
        graphId = crypto.randomBytes(32).toString('hex')
        return require('../../../../src/graph/server')({})
          .then(s => server = s)
      })

      suiteTeardown(() => server.stop())

      suite('add a sample:', () => {
        let sampling, sampled

        setup(() => {
          sampling = crypto.randomBytes(32).toString('hex')
          sampled = crypto.randomBytes(32).toString('hex')

          return client.post({
            url: `/${graphId}/samples`,
            body: { sampling, sampled }
          })
        })

        suite('get sampled with match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/sampled/${sampling}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { sampling, sampled } ])
          )
        })

        suite('get sampled without match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/sampled/${sampled}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('attempt to get sampled with invalid graphId:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}f/sampled/${sampling}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to get sampled with invalid sampling:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/sampled/${new Array(65).join('g')}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "sampling" fails because ["sampling" must only contain hexadecimal characters]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'sampling' ]
            })
          })
        })

        suite('get sampling with match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/sampling/${sampled}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [ { sampling, sampled } ])
          )
        })

        suite('get sampling without match:', () => {
          let result

          setup(() =>
            client.get(`/${graphId}/sampling/${sampling}`)
              .then(r => result = r)
          )

          test('result was correct', () =>
            assert.deepEqual(result, [])
          )
        })

        suite('attempt to get sampling with invalid graphId:', () => {
          let result, error

          setup(() =>
            client.get(`/${new Array(65).join('x')}/sampling/${sampled}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" must only contain hexadecimal characters]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to get sampling with invalid sampled:', () => {
          let result, error

          setup(() =>
            client.get(`/${graphId}/sampling/${sampled.substr(0, 63)}`)
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result was undefined', () =>
            assert.isUndefined(result)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "sampled" fails because ["sampled" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'sampled' ]
            })
          })
        })

        suite('remove a sample:', () => {
          setup(() =>
            client.delete({
              url: `/${graphId}/samples`,
              body: { sampling, sampled }
            })
          )

          suite('get sampled:', () => {
            let result

            setup(() =>
              client.get(`/${graphId}/sampled/${sampling}`)
                .then(r => result = r)
            )

            test('result was correct', () =>
              assert.deepEqual(result, [])
            )
          })

          suite('get sampling:', () => {
            let result

            setup(() =>
              client.get(`/${graphId}/sampling/${sampled}`)
                .then(r => result = r)
            )

            test('result was correct', () =>
              assert.deepEqual(result, [])
            )
          })
        })

        suite('attempt to remove a sample with with invalid graphId:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}0/samples`,
              body: {
                sampling: crypto.randomBytes(32).toString('hex'),
                sampled: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.instanceOf(error, Error)
            assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'params',
              keys: [ 'graph' ]
            })
          })
        })

        suite('attempt to remove a sample with with invalid sampling:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/samples`,
              body: {
                sampling: crypto.randomBytes(32).toString('hex').substr(0, 63),
                sampled: crypto.randomBytes(32).toString('hex')
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "sampling" fails because ["sampling" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'sampling' ]
            })
          })
        })

        suite('attempt to remove a sample with with invalid sampled:', () => {
          let error

          setup(() =>
            client.delete({
              url: `/${graphId}/samples`,
              body: {
                sampling: crypto.randomBytes(32).toString('hex'),
                sampled: `0${crypto.randomBytes(32).toString('hex')}`
              }
            })
            .catch(e => error = e)
          )

          test('error was correct', () => {
            assert.equal(error.message, 'Bad Request: child "sampled" fails because ["sampled" length must be 64 characters long]')
            assert.equal(error.status, 400)
            assert.deepEqual(error.validation, {
              source: 'payload',
              keys: [ 'sampled' ]
            })
          })
        })
      })

      suite('attempt to add a sample with with invalid graphId:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId.substr(0, 63)}/samples`,
            body: {
              sampling: crypto.randomBytes(32).toString('hex'),
              sampled: crypto.randomBytes(32).toString('hex')
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.instanceOf(error, Error)
          assert.equal(error.message, 'Bad Request: child "graph" fails because ["graph" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'params',
            keys: [ 'graph' ]
          })
        })
      })

      suite('attempt to add a sample with with invalid sampling:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/samples`,
            body: {
              sampling: `${crypto.randomBytes(32).toString('hex')}f`,
              sampled: crypto.randomBytes(32).toString('hex')
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "sampling" fails because ["sampling" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'sampling' ]
          })
        })
      })

      suite('attempt to add a sample with with invalid sampled:', () => {
        let error

        setup(() =>
          client.post({
            url: `/${graphId}/samples`,
            body: {
              sampling: crypto.randomBytes(32).toString('hex'),
              sampled: crypto.randomBytes(32).toString('hex').substr(0, 63)
            }
          })
          .catch(e => error = e)
        )

        test('error was correct', () => {
          assert.equal(error.message, 'Bad Request: child "sampled" fails because ["sampled" length must be 64 characters long]')
          assert.equal(error.status, 400)
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'sampled' ]
          })
        })
      })
    })
  })
})

