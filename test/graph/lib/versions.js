// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert

// Uncomment and pass enableVerboseLogging to graph.initialise when you want to debug
/*
const enableVerboseLogging = {
  log: require('bunyan').createLogger({
    name: 'graph/versions',
    level: 'TRACE'
  })
};
*/

suite('graph/versions:', () => {
  let initialise, graphFoo

  suiteSetup(() => {
    initialise = require('../../../src/graph/lib')
    graphFoo = initialise('foo', {})
  })

  test('deleteVersion method is defined', () =>
    assert.isFunction(graphFoo.deleteVersion)
  )

  test('deleteVersion does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )
  )

  test('deleteVersion throws with invalid original argument', () =>
    assert.throws(() =>
      graphFoo.deleteVersion({ toString: () => '__TEST DATA__ track 1' }, '__TEST DATA__ track 2')
    )
  )

  test('deleteVersion throws with invalid derived argument', () =>
    assert.throws(() =>
      graphFoo.deleteVersion('__TEST DATA__ track 1', '')
    )
  )

  test('addVersion method is defined', () =>
    assert.isFunction(graphFoo.addVersion)
  )

  test('addVersion does not throw', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        .then(() =>
          graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        )
        .then(done)
    )
  )

  test('addVersion throws with invalid original argument', () =>
    assert.throws(() =>
      graphFoo.addVersion('', '__TEST DATA__ track 2')
    )
  )

  test('addVersion throws with invalid derived argument', () =>
    assert.throws(() =>
      graphFoo.addVersion('__TEST DATA__ track 1', { toString: () => '__TEST DATA__ track 2' })
    )
  )

  test('addVersion throws with invalid versionType argument', () =>
    assert.throws(() =>
      graphFoo.addVersion('', '__TEST DATA__ track 2', 'wibble')
    )
  )

  test('addVersion does not throw with valid versionType arguments', () =>
    Promise.all(
      [ 'take', 'remaster', 'remix', 'edit', 'cover', 'live' ].map(versionType =>
        new Promise(resolve =>
          assert.doesNotThrow(() =>
            graphFoo.addVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2', versionType)
              .then(() =>
                graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
              )
              .then(resolve)
          )
        )
      )
    )
  )

  test('getVersions method is defined', () =>
    assert.isFunction(graphFoo.getVersions)
  )

  test('getVersions throws with invalid track argument', () =>
    assert.throws(() =>
      graphFoo.getVersions('')
    )
  )

  test('getVersions throws with invalid versionType argument', () =>
    assert.throws(() =>
      graphFoo.getVersions('__TEST DATA__ track 1', '')
    )
  )

  test('getVersions does not throw with valid versionType arguments', () =>
    [ 'take', 'remaster', 'remix', 'edit', 'cover', 'live' ].forEach(versionType =>
      assert.doesNotThrow(() =>
        graphFoo.getVersions('__TEST DATA__ track 1', versionType)
      )
    )
  )

  suite('getVersions:', () => {
    let versionsFoo

    suiteSetup(() =>
      graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
        versionsFoo = v
      )
    )

    test('getVersions resolves to array', () =>
      assert.isArray(versionsFoo)
    )

    test('versions array is empty', () =>
      assert.lengthOf(versionsFoo, 0)
    )
  })

  suite('addVersion with default versionType:', () => {
    setup(() =>
      graphFoo.addVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )

    teardown(() =>
      graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )

    suite('getVersions without versionType:', () => {
      let versionsFoo

      setup(() =>
        graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
          versionsFoo = v
        )
      )

      test('versions array has one item', () =>
        assert.lengthOf(versionsFoo, 1)
      )

      test('versions array item is object', () =>
        assert.isObject(versionsFoo[0])
      )

      test('versions array item has four properties', () =>
        assert.lengthOf(Object.keys(versionsFoo[0]), 4)
      )

      test('versions array item has correct subject property', () =>
        assert.strictEqual(versionsFoo[0].subject, '__TEST DATA__ track 2')
      )

      test('versions array item has correct predicate property', () =>
        assert.strictEqual(versionsFoo[0].predicate, 'is version of')
      )

      test('versions array item has correct object property', () =>
        assert.strictEqual(versionsFoo[0].object, '__TEST DATA__ track 1')
      )

      test('versions array item has correct versionType property', () =>
        assert.strictEqual(versionsFoo[0].versionType, 'take')
      )

      suite('addVersion with different versionType:', () => {
        setup(() =>
          graphFoo.addVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2', 'remaster')
        )

        teardown(() =>
          graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        )

        suite('getVersions with matching versionType:', () => {
          let versionsFooInner

          setup(() =>
            graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
              versionsFooInner = v
            )
          )

          test('versions array has one item', () =>
            assert.lengthOf(versionsFooInner, 1)
          )

          test('versions array item has correct subject property', () =>
            assert.strictEqual(versionsFooInner[0].subject, '__TEST DATA__ track 2')
          )

          test('versions array item has correct object property', () =>
            assert.strictEqual(versionsFooInner[0].object, '__TEST DATA__ track 1')
          )

          test('versions array item has correct versionType property', () =>
            assert.strictEqual(versionsFooInner[0].versionType, 'remaster')
          )
        })

        suite('getVersions with wrong versionType:', () => {
          let versionsFooInner

          suiteSetup(() =>
            graphFoo.getVersions('__TEST DATA__ track 1', 'take').then((v) =>
              versionsFooInner = v
            )
          )

          test('versions array is empty', () =>
            assert.lengthOf(versionsFooInner, 0)
          )
        })

        suite('getVersions without versionType:', () => {
          let versionsFooInner

          setup(() =>
            graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
              versionsFooInner = v
            )
          )

          test('versions array has one item', () =>
            assert.lengthOf(versionsFooInner, 1)
          )

          test('versions array item has correct versionType property', () =>
            assert.strictEqual(versionsFooInner[0].versionType, 'remaster')
          )
        })

        suite('addVersion with different versionType:', () => {
          setup(() =>
            graphFoo.addVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2', 'remix')
          )

          teardown(() =>
            graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2')
          )

          suite('getVersions with matching versionType:', () => {
            let versionsFooInner

            setup(() =>
              graphFoo.getVersions('__TEST DATA__ track 1', 'remix').then((v) =>
                versionsFooInner = v
              )
            )

            test('versions array has one item', () =>
              assert.lengthOf(versionsFooInner, 1)
            )

            test('versions array item has correct versionType property', () =>
              assert.strictEqual(versionsFooInner[0].versionType, 'remix')
            )
          })

          suite('getVersions with wrong versionType:', () => {
            let versionsFooInner

            suiteSetup(() =>
              graphFoo.getVersions('__TEST DATA__ track 1', 'remaster').then((v) =>
                versionsFooInner = v
              )
            )

            test('versions array is empty', () =>
              assert.lengthOf(versionsFooInner, 0)
            )
          })
        })
      })
    })

    suite('getVersions with wrong versionType:', () => {
      let versionsFoo

      suiteSetup(() =>
        graphFoo.getVersions('__TEST DATA__ track 1', 'remix').then((v) =>
          versionsFoo = v
        )
      )

      test('versions array is empty', () =>
        assert.lengthOf(versionsFoo, 0)
      )
    })

    suite('getVersions with matching versionType:', () => {
      let versionsFoo

      setup(() =>
        graphFoo.getVersions('__TEST DATA__ track 1', 'take').then((v) =>
          versionsFoo = v
        )
      )

      test('versions array has one item', () =>
        assert.lengthOf(versionsFoo, 1)
      )

      test('versions array item has correct versionType property', () =>
        assert.strictEqual(versionsFoo[0].versionType, 'take')
      )
    })

    suite('addVersion in different direction:', () => {
      suiteSetup(() =>
        graphFoo.addVersion('__TEST DATA__ track 0', '__TEST DATA__ track 1', 'edit')
      )

      suiteTeardown(() =>
        graphFoo.deleteVersion('__TEST DATA__ track 0', '__TEST DATA__ track 1')
      )

      suite('getVersions with parent and child:', () => {
        let versionsFoo

        setup(() =>
          graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
            versionsFoo = v
          )
        )

        test('versions array has two items', () =>
          assert.lengthOf(versionsFoo, 2)
        )

        test('second versions array item has correct subject property', () =>
          assert.strictEqual(versionsFoo[0].subject, '__TEST DATA__ track 1')
        )

        test('second versions array item has correct object property', () =>
          assert.strictEqual(versionsFoo[0].object, '__TEST DATA__ track 0')
        )

        test('second versions array item has correct versionType property', () =>
          assert.strictEqual(versionsFoo[0].versionType, 'edit')
        )

        test('first versions array item has correct subject property', () =>
          assert.strictEqual(versionsFoo[1].subject, '__TEST DATA__ track 2')
        )

        test('first versions array item has correct object property', () =>
          assert.strictEqual(versionsFoo[1].object, '__TEST DATA__ track 1')
        )

        test('first versions array item has correct versionType property', () =>
          assert.strictEqual(versionsFoo[1].versionType, 'take')
        )
      })

      suite('addVersion with peer:', () => {
        suiteSetup(() =>
          graphFoo.addVersion('__TEST DATA__ track 0', '__TEST DATA__ track 1b', 'cover')
        )

        suiteTeardown(() =>
          graphFoo.deleteVersion('__TEST DATA__ track 0', '__TEST DATA__ track 1b')
        )

        suite('getVersions with parent, child and peer:', () => {
          let versionsFoo

          setup(() =>
            graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
              versionsFoo = v
            )
          )

          test('versions array has three items', () =>
            assert.lengthOf(versionsFoo, 3)
          )

          test('second versions array item has correct subject property', () =>
            assert.strictEqual(versionsFoo[0].subject, '__TEST DATA__ track 1')
          )

          test('second versions array item has correct object property', () =>
            assert.strictEqual(versionsFoo[0].object, '__TEST DATA__ track 0')
          )

          test('second versions array item has correct versionType property', () =>
            assert.strictEqual(versionsFoo[0].versionType, 'edit')
          )

          test('first versions array item has correct subject property', () =>
            assert.strictEqual(versionsFoo[1].subject, '__TEST DATA__ track 2')
          )

          test('first versions array item has correct object property', () =>
            assert.strictEqual(versionsFoo[1].object, '__TEST DATA__ track 1')
          )

          test('first versions array item has correct versionType property', () =>
            assert.strictEqual(versionsFoo[1].versionType, 'take')
          )

          test('third versions array item has correct subject property', () =>
            assert.strictEqual(versionsFoo[2].subject, '__TEST DATA__ track 1b')
          )

          test('third versions array item has correct object property', () =>
            assert.strictEqual(versionsFoo[2].object, '__TEST DATA__ track 0')
          )

          test('third versions array item has correct versionType property', () =>
            assert.strictEqual(versionsFoo[2].versionType, 'cover')
          )
        })

        suite('addVersion with grandparent:', () => {
          suiteSetup(() =>
            graphFoo.addVersion('__TEST DATA__ track 2', '__TEST DATA__ track 3')
          )

          suiteTeardown(() =>
            graphFoo.deleteVersion('__TEST DATA__ track 2', '__TEST DATA__ track 3')
          )

          suite('getVersions with parent, child, peer and grandparent:', () => {
            let versionsFoo

            setup(() =>
              graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                versionsFoo = v
              )
            )

            test('versions array has three items', () =>
              assert.lengthOf(versionsFoo, 3)
            )
          })
        })

        suite('addVersion with grandchild:', () => {
          suiteSetup(() =>
            graphFoo.addVersion('__TEST DATA__ track -1', '__TEST DATA__ track 0')
          )

          suiteTeardown(() =>
            graphFoo.deleteVersion('__TEST DATA__ track -1', '__TEST DATA__ track 0')
          )

          suite('getVersions with parent, child, peer and grandchild:', () => {
            let versionsFoo

            setup(() =>
              graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                versionsFoo = v
              )
            )

            test('versions array has three items', () =>
              assert.lengthOf(versionsFoo, 3)
            )
          })
        })

        suite('addVersion with parent:', () => {
          suiteSetup(() =>
            graphFoo.addVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2b')
          )

          suiteTeardown(() =>
            graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 2b')
          )

          suite('getVersions with two parents, child and peer:', () => {
            let versionsFoo

            setup(() =>
              graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                versionsFoo = v
              )
            )

            test('versions array has four items', () =>
              assert.lengthOf(versionsFoo, 4)
            )
          })
        })

        suite('addVersion with child:', () => {
          suiteSetup(() =>
            graphFoo.addVersion('__TEST DATA__ track 0b', '__TEST DATA__ track 1')
          )

          suiteTeardown(() =>
            graphFoo.deleteVersion('__TEST DATA__ track 0b', '__TEST DATA__ track 1')
          )

          suite('getVersions with parent, two children and peer:', () => {
            let versionsFoo

            setup(() =>
              graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                versionsFoo = v
              )
            )

            test('versions array has four items', () =>
              assert.lengthOf(versionsFoo, 4)
            )
          })

          suite('addVersion with peer:', () => {
            suiteSetup(() =>
              graphFoo.addVersion('__TEST DATA__ track 0b', '__TEST DATA__ track 1c')
            )

            suiteTeardown(() =>
              graphFoo.deleteVersion('__TEST DATA__ track 0b', '__TEST DATA__ track 1c')
            )

            suite('getVersions with parent, child and two peers:', () => {
              let versionsFoo

              setup(() =>
                graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                  versionsFoo = v
                )
              )

              test('versions array has five items', () =>
                assert.lengthOf(versionsFoo, 5)
              )
            })
          })

          suite('addVersion with duplicate parent:', () => {
            suiteSetup(() =>
              graphFoo.addVersion('__TEST DATA__ track 1', '__TEST DATA__ track 0')
            )

            suiteTeardown(() =>
              graphFoo.deleteVersion('__TEST DATA__ track 1', '__TEST DATA__ track 0')
            )

            suite('getVersions with parent, child and two peers:', () => {
              let versionsFoo

              setup(() =>
                graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                  versionsFoo = v
                )
              )

              test('versions array has four items', () =>
                assert.lengthOf(versionsFoo, 4)
              )
            })
          })

          suite('addVersion with duplicate child:', () => {
            suiteSetup(() =>
              graphFoo.addVersion('__TEST DATA__ track 2', '__TEST DATA__ track 1')
            )

            suiteTeardown(() =>
              graphFoo.deleteVersion('__TEST DATA__ track 2', '__TEST DATA__ track 1')
            )

            suite('getVersions with parent, child and two peers:', () => {
              let versionsFoo

              setup(() =>
                graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                  versionsFoo = v
                )
              )

              test('versions array has four items', () =>
                assert.lengthOf(versionsFoo, 4)
              )
            })
          })

          suite('addVersion with duplicate peer:', () => {
            suiteSetup(() =>
              graphFoo.addVersion('__TEST DATA__ track 0b', '__TEST DATA__ track 2')
            )

            suiteTeardown(() =>
              graphFoo.deleteVersion('__TEST DATA__ track 0b', '__TEST DATA__ track 2')
            )

            suite('getVersions with parent, child and two peers:', () => {
              let versionsFoo

              setup(() =>
                graphFoo.getVersions('__TEST DATA__ track 1').then((v) =>
                  versionsFoo = v
                )
              )

              test('versions array has four items', () =>
                assert.lengthOf(versionsFoo, 4)
              )
            })
          })
        })
      })
    })
  })
})

