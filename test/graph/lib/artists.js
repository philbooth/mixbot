// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert

// Uncomment and pass enableVerboseLogging to graph.initialise when you want to debug
/*
const enableVerboseLogging = {
  log: require('bunyan').createLogger({
    name: 'graph/artists',
    level: 'TRACE'
  })
};
*/

suite('graph/artists:', () => {
  let initialise, graphFoo

  suiteSetup(() => {
    initialise = require('../../../src/graph/lib')
    graphFoo = initialise('foo', {})
  })

  test('deleteArtist method is defined', () =>
    assert.isFunction(graphFoo.deleteArtist)
  )

  test('deleteArtist does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1')
    )
  )

  test('deleteArtist throws with invalid track argument', () =>
    assert.throws(() =>
      graphFoo.deleteArtist({ toString: () => '__TEST DATA__ track 1' }, '__TEST DATA__ artist 1')
    )
  )

  test('deleteArtist throws with invalid artist argument', () =>
    assert.throws(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '')
    )
  )

  test('deleteArtist throws with invalid role argument', () =>
    assert.throws(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'foo')
    )
  )

  test('deleteArtist does not throw with contributed_to role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'contributed to')
    )
  )

  test('deleteArtist does not throw with wrote role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'wrote')
    )
  )

  test('deleteArtist does not throw with produced role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'produced')
    )
  )

  test('deleteArtist does not throw with performed role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'performed')
    )
  )

  test('deleteArtist does not throw with remixed role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'remixed')
    )
  )

  test('deleteArtist does not throw with remastered role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'remastered')
    )
  )

  test('deleteArtist does not throw with edited role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'edited')
    )
  )

  test('deleteArtist does not throw with featured_on role argument', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'featured on')
    )
  )

  test('addArtist method is defined', () =>
    assert.isFunction(graphFoo.addArtist)
  )

  test('addArtist does not throw', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1')
        )
        .then(done)
    )
  )

  test('addArtist throws with invalid track argument', () =>
    assert.throws(() =>
      graphFoo.addArtist('', '__TEST DATA__ artist 1')
    )
  )

  test('addArtist throws with invalid artist argument', () =>
    assert.throws(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', { toString: () => '__TEST DATA__ artist 1' })
    )
  )

  test('addArtist throws with invalid role argument', () =>
    assert.throws(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'wibble')
    )
  )

  test('addArtist does not throw with contributed_to role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'contributed to')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'contributed to')
        )
        .then(done)
    )
  )

  test('addArtist does not throw with wrote role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'wrote')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'wrote')
        )
        .then(done)
    )
  )

  test('addArtist does not throw with produced role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'produced')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'produced')
        )
        .then(done)
    )
  )

  test('addArtist does not throw with performed role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'performed')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'performed')
        )
        .then(done)
    )
  )

  test('addArtist does not throw with remixed role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'remixed')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'remixed')
        )
        .then(done)
    )
  )

  test('addArtist does not throw with remastered role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'remastered')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'remastered')
        )
        .then(done)
    )
  )

  test('addArtist does not throw with edited role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'edited')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'edited')
        )
        .then(done)
    )
  )

  test('addArtist does not throw with featured_on role argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'featured on')
        .then(() =>
          graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1', 'featured on')
        )
        .then(done)
    )
  )

  test('getArtists method is defined', () =>
    assert.isFunction(graphFoo.getArtists)
  )

  test('getArtists does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.getArtists('__TEST DATA__ track 1')
    )
  )

  test('getArtists throws with invalid track argument', () =>
    assert.throws(() =>
      graphFoo.getArtists('')
    )
  )

  test('getTracks method is defined', () =>
    assert.isFunction(graphFoo.getTracks)
  )

  test('getTracks does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.getTracks('__TEST DATA__ artist 1')
    )
  )

  test('getTracks throws with invalid artist argument', () =>
    assert.throws(() =>
      graphFoo.getTracks('')
    )
  )

  suite('getArtists:', () => {
    let artistsFoo

    suiteSetup(() =>
      graphFoo.getArtists('__TEST DATA__ track 1').then((m) =>
        artistsFoo = m
      )
    )

    test('getArtists resolves to array', () =>
      assert.isArray(artistsFoo)
    )

    test('artists array is empty', () =>
      assert.lengthOf(artistsFoo, 0)
    )
  })

  suite('getTracks:', () => {
    let artistsFoo

    suiteSetup(() =>
      graphFoo.getTracks('__TEST DATA__ track 2').then((m) =>
        artistsFoo = m
      )
    )

    test('getTracks resolves to array', () =>
      assert.isArray(artistsFoo)
    )

    test('artists array is empty', () =>
      assert.lengthOf(artistsFoo, 0)
    )
  })

  suite('addArtist:', () => {
    setup(() =>
      graphFoo.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1')
    )

    teardown(() =>
      graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1')
    )

    suite('getArtists with match:', () => {
      let artistsFoo

      setup(() =>
        graphFoo.getArtists('__TEST DATA__ track 1').then((m) =>
          artistsFoo = m
        )
      )

      test('artists array has one item', () =>
        assert.lengthOf(artistsFoo, 1)
      )

      test('artists array item is object', () =>
        assert.isObject(artistsFoo[0])
      )

      test('artists array item has three properties', () =>
        assert.lengthOf(Object.keys(artistsFoo[0]), 4)
      )

      test('artists array item has correct subject property', () =>
        assert.strictEqual(artistsFoo[0].subject, '__TEST DATA__ artist 1')
      )

      test('artists array item has correct predicate property', () =>
        assert.strictEqual(artistsFoo[0].predicate, 'contributed to')
      )

      test('artists array item has correct object property', () =>
        assert.strictEqual(artistsFoo[0].object, '__TEST DATA__ track 1')
      )

      test('artists array item has correct role property', () => {
        assert.isArray(artistsFoo[0].roles)
        assert.lengthOf(artistsFoo[0].roles, 1)
        assert.strictEqual(artistsFoo[0].roles[0], 'contributed to')
      })
    })

    suite('getArtists without match:', () => {
      let artistsFoo

      suiteSetup(() =>
        graphFoo.getArtists('__TEST DATA__ track 2').then((m) =>
          artistsFoo = m
        )
      )

      test('artists array is empty', () =>
        assert.lengthOf(artistsFoo, 0)
      )
    })

    suite('getTracks with match:', () => {
      let tracksFoo

      setup(() =>
        graphFoo.getTracks('__TEST DATA__ artist 1').then((m) =>
          tracksFoo = m
        )
      )

      test('tracks array has one item', () =>
        assert.lengthOf(tracksFoo, 1)
      )

      test('tracks array item is object', () =>
        assert.isObject(tracksFoo[0])
      )

      test('tracks array item has three properties', () =>
        assert.lengthOf(Object.keys(tracksFoo[0]), 4)
      )

      test('tracks array item has correct subject property', () =>
        assert.strictEqual(tracksFoo[0].subject, '__TEST DATA__ artist 1')
      )

      test('tracks array item has correct predicate property', () =>
        assert.strictEqual(tracksFoo[0].predicate, 'contributed to')
      )

      test('tracks array item has correct object property', () =>
        assert.strictEqual(tracksFoo[0].object, '__TEST DATA__ track 1')
      )

      test('tracks array item has correct role property', () => {
        assert.isArray(tracksFoo[0].roles)
        assert.lengthOf(tracksFoo[0].roles, 1)
        assert.strictEqual(tracksFoo[0].roles[0], 'contributed to')
      })
    })

    suite('getTracks without match:', () => {
      let tracksFoo

      suiteSetup(() =>
        graphFoo.getTracks('__TEST DATA__ artist 1').then((m) =>
          tracksFoo = m
        )
      )

      test('artists array is empty', () =>
        assert.lengthOf(tracksFoo, 0)
      )
    })

    suite('deleteArtist:', () => {
      suiteSetup(() =>
        graphFoo.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1')
      )

      suite('getArtists:', () => {
        let artistsFoo

        suiteSetup(() =>
          graphFoo.getArtists('__TEST DATA__ track 1').then((m) =>
            artistsFoo = m
          )
        )

        test('artists array is empty', () =>
          assert.lengthOf(artistsFoo, 0)
        )
      })

      suite('getTracks:', () => {
        let tracksFoo

        suiteSetup(() =>
          graphFoo.getTracks('__TEST DATA__ artist 1').then((m) =>
            tracksFoo = m
          )
        )

        test('tracks array is empty', () =>
          assert.lengthOf(tracksFoo, 0)
        )
      })
    })

    suite('initialise:', () => {
      let graphBar

      suiteSetup(() =>
        graphBar = initialise('bar', {})
      )

      suite('addArtist:', () => {
        suiteSetup(() =>
          graphBar.addArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1b')
        )

        suiteTeardown(() =>
          graphBar.deleteArtist('__TEST DATA__ track 1', '__TEST DATA__ artist 1b')
        )

        suite('getArtists with match:', () => {
          let artistsBar

          setup(() =>
            graphBar.getArtists('__TEST DATA__ track 1').then((m) =>
              artistsBar = m
            )
          )

          test('artists array has two items', () =>
            assert.lengthOf(artistsBar, 2)
          )

          test('first artists array item has correct subject property', () =>
            assert.strictEqual(artistsBar[0].subject, '__TEST DATA__ artist 1')
          )

          test('second artists array item has correct subject property', () =>
            assert.strictEqual(artistsBar[1].subject, '__TEST DATA__ artist 1b')
          )
        })

        suite('addArtist on previous graph then getTracks with match:', () => {
          let artistsFooInner

          suiteSetup(() =>
            graphFoo.addArtist('__TEST DATA__ track 1b', '__TEST DATA__ artist 1b')
              .then(() =>
                graphFoo.getTracks('__TEST DATA__ artist 1b').then(m =>
                  artistsFooInner = m
                )
              )
          )

          suiteTeardown(() =>
            graphFoo.deleteArtist('__TEST DATA__ track 1b', '__TEST DATA__ artist 1b')
          )

          test('artists array has two items', () =>
            assert.lengthOf(artistsFooInner, 2)
          )

          test('first artists array item has correct object property', () =>
            assert.strictEqual(artistsFooInner[0].object, '__TEST DATA__ track 1')
          )

          test('second artists array item has correct object property', () =>
            assert.strictEqual(artistsFooInner[1].object, '__TEST DATA__ track 1b')
          )
        })
      })
    })
  })
})

