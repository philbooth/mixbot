// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert

// Uncomment and pass enableVerboseLogging to graph.initialise when you want to debug
/*
const enableVerboseLogging = {
  log: require('bunyan').createLogger({
    name: 'graph/mixes',
    level: 'TRACE'
  })
};
*/

suite('graph/mixes:', () => {
  let initialise, graphFoo

  suiteSetup(() => {
    initialise = require('../../../src/graph/lib')
    graphFoo = initialise('foo', {})
  })

  test('deleteMix method is defined', () =>
    assert.isFunction(graphFoo.deleteMix)
  )

  test('deleteMix does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )
  )

  test('deleteMix throws with invalid from argument', () =>
    assert.throws(() =>
      graphFoo.deleteMix({ toString: () => '__TEST DATA__ track 1' }, '__TEST DATA__ track 2')
    )
  )

  test('deleteMix throws with invalid to argument', () =>
    assert.throws(() =>
      graphFoo.deleteMix('__TEST DATA__ track 1', '')
    )
  )

  test('addMix method is defined', () =>
    assert.isFunction(graphFoo.addMix)
  )

  test('addMix does not throw', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        .then(() =>
          graphFoo.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        )
        .then(() =>
          done()
        )
    )
  )

  test('addMix throws with invalid from argument', () =>
    assert.throws(() =>
      graphFoo.addMix('', '__TEST DATA__ track 2')
    )
  )

  test('addMix throws with invalid to argument', () =>
    assert.throws(() =>
      graphFoo.addMix('__TEST DATA__ track 1', { toString: () => '__TEST DATA__ track 2' })
    )
  )

  test('addMix throws with invalid weight argument', () =>
    assert.throws(() =>
      graphFoo.addMix('__TEST DATA__ track 1', '__TEST DATA__ track 2', 101)
    )
  )

  test('addMix does not throw with valid weight argument', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addMix('__TEST DATA__ track 1', '__TEST DATA__ track 2', 100)
        .then(() =>
          graphFoo.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        )
        .then(() =>
          done()
        )
    )
  )

  test('getMixesFrom method is defined', () =>
    assert.isFunction(graphFoo.getMixesFrom)
  )

  test('getMixesFrom does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.getMixesFrom('__TEST DATA__ track 1', 1)
    )
  )

  test('getMixesFrom throws with invalid from argument', () =>
    assert.throws(() =>
      graphFoo.getMixesFrom('')
    )
  )

  test('getMixesFrom throws with invalid weight argument', () =>
    assert.throws(() =>
      graphFoo.getMixesFrom('__TEST DATA__ track 1', -1)
    )
  )

  test('getMixesTo method is defined', () =>
    assert.isFunction(graphFoo.getMixesTo)
  )

  test('getMixesTo does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.getMixesTo('__TEST DATA__ track 1', 100)
    )
  )

  test('getMixesTo throws with invalid to argument', () =>
    assert.throws(() =>
      graphFoo.getMixesTo('')
    )
  )

  test('getMixesTo throws with invalid weight argument', () =>
    assert.throws(() =>
      graphFoo.getMixesTo('__TEST DATA__ track 1', 101)
    )
  )

  suite('getMixesFrom:', () => {
    let mixesFoo

    suiteSetup(() =>
      graphFoo.getMixesFrom('__TEST DATA__ track 1').then((m) =>
        mixesFoo = m
      )
    )

    test('getMixesFrom resolves to object', () =>
      assert.isObject(mixesFoo)
    )

    test('mixes object has two properties', () =>
      assert.lengthOf(Object.keys(mixesFoo), 2)
    )

    test('mixes object has master array', () =>
      assert.isArray(mixesFoo.master)
    )

    test('master array is empty', () =>
      assert.lengthOf(mixesFoo.master, 0)
    )

    test('mixes object has local array', () =>
      assert.isArray(mixesFoo.local)
    )

    test('local array is empty', () =>
      assert.lengthOf(mixesFoo.local, 0)
    )
  })

  suite('getMixesTo:', () => {
    let mixesFoo

    suiteSetup(() =>
      graphFoo.getMixesTo('__TEST DATA__ track 2').then((m) =>
        mixesFoo = m
      )
    )

    test('getMixesTo resolves to object', () =>
      assert.isObject(mixesFoo)
    )

    test('mixes object has two properties', () =>
      assert.lengthOf(Object.keys(mixesFoo), 2)
    )

    test('mixes object has master array', () =>
      assert.isArray(mixesFoo.master)
    )

    test('master array is empty', () =>
      assert.lengthOf(mixesFoo.master, 0)
    )

    test('mixes object has local array', () =>
      assert.isArray(mixesFoo.local)
    )

    test('local array is empty', () =>
      assert.lengthOf(mixesFoo.local, 0)
    )
  })

  suite('addMix:', () => {
    setup(() =>
      graphFoo.addMix('__TEST DATA__ track 1', '__TEST DATA__ track 2', 100)
    )

    teardown(() =>
      graphFoo.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )

    suite('getMixesFrom with match:', () => {
      let mixesFoo

      setup(() =>
        graphFoo.getMixesFrom('__TEST DATA__ track 1').then((m) =>
          mixesFoo = m
        )
      )

      test('master array has one item', () =>
        assert.lengthOf(mixesFoo.master, 1)
      )

      test('master array item is object', () =>
        assert.isObject(mixesFoo.master[0])
      )

      test('master array item has five properties', () =>
        assert.lengthOf(Object.keys(mixesFoo.master[0]), 5)
      )

      test('master array item has correct subject property', () =>
        assert.strictEqual(mixesFoo.master[0].subject, '__TEST DATA__ track 1')
      )

      test('master array item has correct predicate property', () =>
        assert.strictEqual(mixesFoo.master[0].predicate, 'mixes into')
      )

      test('master array item has correct object property', () =>
        assert.strictEqual(mixesFoo.master[0].object, '__TEST DATA__ track 2')
      )

      test('master array item has correct aggregateWeight property', () =>
        assert.strictEqual(mixesFoo.master[0].aggregateWeight, 100)
      )

      test('master array item has correct weightCount property', () =>
        assert.strictEqual(mixesFoo.master[0].weightCount, 1)
      )

      test('local array has one item', () =>
        assert.lengthOf(mixesFoo.local, 1)
      )

      test('local array item is object', () =>
        assert.isObject(mixesFoo.local[0])
      )

      test('local array item has four properties', () =>
        assert.lengthOf(Object.keys(mixesFoo.local[0]), 4)
      )

      test('local array item has correct subject property', () =>
        assert.strictEqual(mixesFoo.local[0].subject, '__TEST DATA__ track 1')
      )

      test('local array item has correct predicate property', () =>
        assert.strictEqual(mixesFoo.local[0].predicate, 'mixes into')
      )

      test('local array item has correct object property', () =>
        assert.strictEqual(mixesFoo.local[0].object, '__TEST DATA__ track 2')
      )

      test('local array item has correct weight property', () =>
        assert.strictEqual(mixesFoo.local[0].weight, 100)
      )
    })

    suite('getMixesFrom without match:', () => {
      let mixesFoo

      suiteSetup(() =>
        graphFoo.getMixesFrom('__TEST DATA__ track 2').then((m) =>
          mixesFoo = m
        )
      )

      test('master array is empty', () =>
        assert.lengthOf(mixesFoo.master, 0)
      )

      test('local array is empty', () =>
        assert.lengthOf(mixesFoo.local, 0)
      )
    })

    suite('getMixesTo with match:', () => {
      let mixesFoo

      setup(() =>
        graphFoo.getMixesTo('__TEST DATA__ track 2').then((m) =>
          mixesFoo = m
        )
      )

      test('master array has one item', () =>
        assert.lengthOf(mixesFoo.master, 1)
      )

      test('master array item is object', () =>
        assert.isObject(mixesFoo.master[0])
      )

      test('master array item has five properties', () =>
        assert.lengthOf(Object.keys(mixesFoo.master[0]), 5)
      )

      test('master array item has correct subject property', () =>
        assert.strictEqual(mixesFoo.master[0].subject, '__TEST DATA__ track 1')
      )

      test('master array item has correct predicate property', () =>
        assert.strictEqual(mixesFoo.master[0].predicate, 'mixes into')
      )

      test('master array item has correct object property', () =>
        assert.strictEqual(mixesFoo.master[0].object, '__TEST DATA__ track 2')
      )

      test('master array item has correct aggregateWeight property', () =>
        assert.strictEqual(mixesFoo.master[0].aggregateWeight, 100)
      )

      test('master array item has correct weightCount property', () =>
        assert.strictEqual(mixesFoo.master[0].weightCount, 1)
      )

      test('local array has one item', () =>
        assert.lengthOf(mixesFoo.local, 1)
      )

      test('local array item is object', () =>
        assert.isObject(mixesFoo.local[0])
      )

      test('local array item has four properties', () =>
        assert.lengthOf(Object.keys(mixesFoo.local[0]), 4)
      )

      test('local array item has correct subject property', () =>
        assert.strictEqual(mixesFoo.local[0].subject, '__TEST DATA__ track 1')
      )

      test('local array item has correct predicate property', () =>
        assert.strictEqual(mixesFoo.local[0].predicate, 'mixes into')
      )

      test('local array item has correct object property', () =>
        assert.strictEqual(mixesFoo.local[0].object, '__TEST DATA__ track 2')
      )

      test('local array item has correct weight property', () =>
        assert.strictEqual(mixesFoo.local[0].weight, 100)
      )
    })

    suite('getMixesTo without match:', () => {
      let mixesFoo

      suiteSetup(() =>
        graphFoo.getMixesTo('__TEST DATA__ track 1').then((m) =>
          mixesFoo = m
        )
      )

      test('master array is empty', () =>
        assert.lengthOf(mixesFoo.master, 0)
      )

      test('local array is empty', () =>
        assert.lengthOf(mixesFoo.local, 0)
      )
    })

    suite('deleteMix:', () => {
      suiteSetup(() =>
        graphFoo.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
      )

      suite('getMixesFrom:', () => {
        let mixesFoo

        suiteSetup(() =>
          graphFoo.getMixesFrom('__TEST DATA__ track 1').then((m) =>
            mixesFoo = m
          )
        )

        test('master array is empty', () =>
          assert.lengthOf(mixesFoo.master, 0)
        )

        test('local array is empty', () =>
          assert.lengthOf(mixesFoo.local, 0)
        )
      })

      suite('getMixesTo:', () => {
        let mixesFoo

        suiteSetup(() =>
          graphFoo.getMixesTo('__TEST DATA__ track 2').then((m) =>
            mixesFoo = m
          )
        )

        test('master array is empty', () =>
          assert.lengthOf(mixesFoo.master, 0)
        )

        test('local array is empty', () =>
          assert.lengthOf(mixesFoo.local, 0)
        )
      })
    })

    suite('initialise:', () => {
      let graphBar

      suiteSetup(() =>
        graphBar = initialise('bar', {})
      )

      suite('addMix:', () => {
        suiteSetup(() =>
          graphBar.addMix('__TEST DATA__ track 1', '__TEST DATA__ track 2', 70)
        )

        suiteTeardown(() =>
          graphBar.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        )

        suite('getMixesFrom with match:', () => {
          let mixesBar

          setup(() =>
            graphBar.getMixesFrom('__TEST DATA__ track 1').then((m) =>
              mixesBar = m
            )
          )

          test('master array has one item', () =>
            assert.lengthOf(mixesBar.master, 1)
          )

          test('master array item has correct aggregateWeight property', () =>
            assert.strictEqual(mixesBar.master[0].aggregateWeight, 170)
          )

          test('master array item has correct weightCount property', () =>
            assert.strictEqual(mixesBar.master[0].weightCount, 2)
          )

          test('local array has one item', () =>
            assert.lengthOf(mixesBar.local, 1)
          )

          test('local array item has correct weight property', () =>
            assert.strictEqual(mixesBar.local[0].weight, 70)
          )
        })

        suite('addMix on previous graph then getMixesFrom with match:', () => {
          let mixesFooInner

          suiteSetup(() =>
            graphFoo.addMix('__TEST DATA__ track 1', '__TEST DATA__ track 2', 50)
              .then(() =>
                graphFoo.getMixesFrom('__TEST DATA__ track 1').then((m) => mixesFooInner = m)
              )
          )

          suiteTeardown(() =>
            graphFoo.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
          )

          test('master array has one item', () =>
            assert.lengthOf(mixesFooInner.master, 1)
          )

          test('master array item has correct aggregateWeight property', () =>
            assert.strictEqual(mixesFooInner.master[0].aggregateWeight, 120)
          )

          test('master array item has correct weightCount property', () =>
            assert.strictEqual(mixesFooInner.master[0].weightCount, 2)
          )

          test('local array has one item', () =>
            assert.lengthOf(mixesFooInner.local, 1)
          )

          test('local array item has correct weight property', () =>
            assert.strictEqual(mixesFooInner.local[0].weight, 50)
          )
        })

        suite('initialise:', () => {
          let graphBaz

          suiteSetup(() =>
            graphBaz = initialise('baz', {})
          )

          suite('addMix:', () => {
            suiteSetup(() =>
              graphBaz.addMix('__TEST DATA__ track 1', '__TEST DATA__ track 2', 70)
            )

            suiteTeardown(() =>
              graphBaz.deleteMix('__TEST DATA__ track 1', '__TEST DATA__ track 2')
            )

            suite('getMixesFrom with match:', () => {
              let mixesBaz

              setup(() =>
                graphBaz.getMixesFrom('__TEST DATA__ track 1').then((m) =>
                  mixesBaz = m
                )
              )

              test('master array has one item', () =>
                assert.lengthOf(mixesBaz.master, 1)
              )

              test('master array item has correct aggregateWeight property', () =>
                assert.strictEqual(mixesBaz.master[0].aggregateWeight, 240)
              )

              test('master array item has correct weightCount property', () =>
                assert.strictEqual(mixesBaz.master[0].weightCount, 3)
              )

              test('local array has one item', () =>
                assert.lengthOf(mixesBaz.local, 1)
              )

              test('local array item has correct weight property', () =>
                assert.strictEqual(mixesBaz.local[0].weight, 70)
              )
            })
          })
        })
      })
    })
  })
})

