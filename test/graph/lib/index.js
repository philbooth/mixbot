// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert

const modulePath = '../../../src/graph/lib'

suite('graph:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  test('require returns function', () =>
    assert.isFunction(require(modulePath))
  )

  suite('require:', () => {
    let initialise

    suiteSetup(() => initialise = require(modulePath))

    test('initialise expects two arguments', () =>
      assert.lengthOf(initialise, 2)
    )

    test('initialise does not throw', () =>
      assert.doesNotThrow(() => initialise('__TEST DATA__ graph 1', {}))
    )

    test('initialise throws with invalid name argument', () =>
      assert.throws(() => initialise('', {}))
    )

    test('initialise throws with invalid options argument', () =>
      assert.throws(() => initialise('__TEST DATA__ graph 1', []))
    )

    suite('initialise:', () => {
      let graph

      suiteSetup(() => graph = initialise('foo', {}))

      test('initialise returns object', () =>
        assert.isObject(graph)
      )

      test('graph object has fifteen methods', () =>
        assert.lengthOf(Object.keys(graph), 15)
      )
    })
  })
})

