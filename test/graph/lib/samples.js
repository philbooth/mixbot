// Copyright © 2015, 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert

// Uncomment and pass enableVerboseLogging to graph.initialise when you want to debug
/*
const enableVerboseLogging = {
  log: require('bunyan').createLogger({
    name: 'graph/samples',
    level: 'TRACE'
  })
};
*/

suite('graph/samples:', () => {
  let initialise, graphFoo

  suiteSetup(() => {
    initialise = require('../../../src/graph/lib')
    graphFoo = initialise('foo', {})
  })

  test('deleteSample method is defined', () =>
    assert.isFunction(graphFoo.deleteSample)
  )

  test('deleteSample does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.deleteSample('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )
  )

  test('deleteSample throws with invalid sampling argument', () =>
    assert.throws(() =>
      graphFoo.deleteSample({ toString: () => '__TEST DATA__ track 1' }, '__TEST DATA__ track 2')
    )
  )

  test('deleteSample throws with invalid sampled argument', () =>
    assert.throws(() =>
      graphFoo.deleteSample('__TEST DATA__ track 1', '')
    )
  )

  test('addSample method is defined', () =>
    assert.isFunction(graphFoo.addSample)
  )

  test('addSample does not throw', (done) =>
    assert.doesNotThrow(() =>
      graphFoo.addSample('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        .then(() =>
          graphFoo.deleteSample('__TEST DATA__ track 1', '__TEST DATA__ track 2')
        )
        .then(done)
    )
  )

  test('addSample throws with invalid sampling argument', () =>
    assert.throws(() =>
      graphFoo.addSample('', '__TEST DATA__ track 2')
    )
  )

  test('addSample throws with invalid sampled argument', () =>
    assert.throws(() =>
      graphFoo.addSample('__TEST DATA__ track 1', { toString: () => '__TEST DATA__ track 2' })
    )
  )

  test('getSampled method is defined', () =>
    assert.isFunction(graphFoo.getSampled)
  )

  test('getSampled does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.getSampled('__TEST DATA__ track 1')
    )
  )

  test('getSampled throws with invalid sampling argument', () =>
    assert.throws(() =>
      graphFoo.getSampled('')
    )
  )

  test('getSampling method is defined', () =>
    assert.isFunction(graphFoo.getSampling)
  )

  test('getSampling does not throw', () =>
    assert.doesNotThrow(() =>
      graphFoo.getSampling('__TEST DATA__ track 1')
    )
  )

  test('getSampling throws with invalid sampled argument', () =>
    assert.throws(() =>
      graphFoo.getSampling('')
    )
  )

  suite('getSampled:', () => {
    let samplesFoo

    suiteSetup(() =>
      graphFoo.getSampled('__TEST DATA__ track 1').then((m) =>
        samplesFoo = m
      )
    )

    test('getSampled resolves to array', () =>
      assert.isArray(samplesFoo)
    )

    test('samples array is empty', () =>
      assert.lengthOf(samplesFoo, 0)
    )
  })

  suite('getSampling:', () => {
    let samplesFoo

    suiteSetup(() =>
      graphFoo.getSampling('__TEST DATA__ track 2').then((m) =>
        samplesFoo = m
      )
    )

    test('getSampling resolves to array', () =>
      assert.isArray(samplesFoo)
    )

    test('samples array is empty', () =>
      assert.lengthOf(samplesFoo, 0)
    )
  })

  suite('addSample:', () => {
    setup(() =>
      graphFoo.addSample('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )

    teardown(() =>
      graphFoo.deleteSample('__TEST DATA__ track 1', '__TEST DATA__ track 2')
    )

    suite('getSampled with match:', () => {
      let samplesFoo

      setup(() =>
        graphFoo.getSampled('__TEST DATA__ track 1').then((m) =>
          samplesFoo = m
        )
      )

      test('samples array has one item', () =>
        assert.lengthOf(samplesFoo, 1)
      )

      test('samples array item is object', () =>
        assert.isObject(samplesFoo[0])
      )

      test('samples array item has three properties', () =>
        assert.lengthOf(Object.keys(samplesFoo[0]), 3)
      )

      test('samples array item has correct subject property', () =>
        assert.strictEqual(samplesFoo[0].subject, '__TEST DATA__ track 1')
      )

      test('samples array item has correct predicate property', () =>
        assert.strictEqual(samplesFoo[0].predicate, 'samples')
      )

      test('samples array item has correct object property', () =>
        assert.strictEqual(samplesFoo[0].object, '__TEST DATA__ track 2')
      )
    })

    suite('getSampled without match:', () => {
      let samplesFoo

      suiteSetup(() =>
        graphFoo.getSampled('__TEST DATA__ track 2').then((m) =>
          samplesFoo = m
        )
      )

      test('samples array is empty', () =>
        assert.lengthOf(samplesFoo, 0)
      )
    })

    suite('getSampling with match:', () => {
      let samplesFoo

      setup(() =>
        graphFoo.getSampling('__TEST DATA__ track 2').then((m) =>
          samplesFoo = m
        )
      )

      test('samples array has one item', () =>
        assert.lengthOf(samplesFoo, 1)
      )

      test('samples array item is object', () =>
        assert.isObject(samplesFoo[0])
      )

      test('samples array item has three properties', () =>
        assert.lengthOf(Object.keys(samplesFoo[0]), 3)
      )

      test('samples array item has correct subject property', () =>
        assert.strictEqual(samplesFoo[0].subject, '__TEST DATA__ track 1')
      )

      test('samples array item has correct predicate property', () =>
        assert.strictEqual(samplesFoo[0].predicate, 'samples')
      )

      test('samples array item has correct object property', () =>
        assert.strictEqual(samplesFoo[0].object, '__TEST DATA__ track 2')
      )
    })

    suite('getSampling without match:', () => {
      let samplesFoo

      suiteSetup(() =>
        graphFoo.getSampling('__TEST DATA__ track 1').then((m) =>
          samplesFoo = m
        )
      )

      test('samples array is empty', () =>
        assert.lengthOf(samplesFoo, 0)
      )
    })

    suite('deleteSample:', () => {
      suiteSetup(() =>
        graphFoo.deleteSample('__TEST DATA__ track 1', '__TEST DATA__ track 2')
      )

      suite('getSampled:', () => {
        let samplesFoo

        suiteSetup(() =>
          graphFoo.getSampled('__TEST DATA__ track 1').then((m) =>
            samplesFoo = m
          )
        )

        test('samples array is empty', () =>
          assert.lengthOf(samplesFoo, 0)
        )
      })

      suite('getSampling:', () => {
        let samplesFoo

        suiteSetup(() =>
          graphFoo.getSampling('__TEST DATA__ track 2').then((m) =>
            samplesFoo = m
          )
        )

        test('samples array is empty', () =>
          assert.lengthOf(samplesFoo, 0)
        )
      })
    })

    suite('initialise:', () => {
      let graphBar

      suiteSetup(() =>
        graphBar = initialise('bar', {})
      )

      suite('addSample:', () => {
        suiteSetup(() =>
          graphBar.addSample('__TEST DATA__ track 1', '__TEST DATA__ track 2b')
        )

        suiteTeardown(() =>
          graphBar.deleteSample('__TEST DATA__ track 1', '__TEST DATA__ track 2b')
        )

        suite('getSampled with match:', () => {
          let samplesBar

          setup(() =>
            graphBar.getSampled('__TEST DATA__ track 1').then((m) =>
              samplesBar = m
            )
          )

          test('samples array has two items', () =>
            assert.lengthOf(samplesBar, 2)
          )

          test('first samples array item has correct object property', () =>
            assert.strictEqual(samplesBar[0].object, '__TEST DATA__ track 2')
          )

          test('second samples array item has correct object property', () =>
            assert.strictEqual(samplesBar[1].object, '__TEST DATA__ track 2b')
          )
        })

        suite('addSample on previous graph then getSampling with match:', () => {
          let samplesFooInner

          suiteSetup(() =>
            graphFoo.addSample('__TEST DATA__ track 1b', '__TEST DATA__ track 2b')
              .then(() =>
                graphFoo.getSampling('__TEST DATA__ track 2b').then((m) => samplesFooInner = m)
              )
          )

          suiteTeardown(() =>
            graphFoo.deleteSample('__TEST DATA__ track 1b', '__TEST DATA__ track 2b')
          )

          test('samples array has two items', () =>
            assert.lengthOf(samplesFooInner, 2)
          )

          test('first samples array item has correct subject property', () =>
            assert.strictEqual(samplesFooInner[0].subject, '__TEST DATA__ track 1')
          )

          test('second samples array item has correct subject property', () =>
            assert.strictEqual(samplesFooInner[1].subject, '__TEST DATA__ track 1b')
          )
        })
      })
    })
  })
})

