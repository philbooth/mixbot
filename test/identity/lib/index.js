// Copyright © 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert

const modulePath = '../../../src/identity/lib'

// Uncomment and pass enableVerboseLogging to identity.initialise when you want to debug
/*
const enableVerboseLogging = {
  log: require('bunyan').createLogger({
    name: 'identity/debug',
    level: 'TRACE'
  })
};
*/

suite('identity:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  test('require returns function', () =>
    assert.isFunction(require(modulePath))
  )

  suite('require:', () => {
    let initialise

    suiteSetup(() => initialise = require(modulePath))

    test('initialise expects one argument', () =>
      assert.lengthOf(initialise, 1)
    )

    test('initialise does not throw', () =>
      assert.doesNotThrow(() => initialise({}))
    )

    test('initialise throws with invalid argument', () =>
      assert.throws(() => initialise([]))
    )

    suite('initialise:', () => {
      let identity

      suiteSetup(() => initialise({}).then(result => identity = result))

      test('initialise returns object', () =>
        assert.isObject(identity)
      )

      test('identity object has four methods', () =>
        assert.lengthOf(Object.keys(identity), 6)
      )

      test('getArtists method is defined', () =>
        assert.isFunction(identity.getArtists)
      )

      test('getArtists method expects one argument', () =>
        assert.lengthOf(identity.getArtists, 1)
      )

      test('setArtist method is defined', () =>
        assert.isFunction(identity.setArtist)
      )

      test('setArtist method expects one argument', () =>
        assert.lengthOf(identity.setArtist, 1)
      )

      test('deleteArtist method is defined', () =>
        assert.isFunction(identity.deleteArtist)
      )

      test('deleteArtist method expects one argument', () =>
        assert.lengthOf(identity.deleteArtist, 1)
      )

      test('setArtist method is defined', () =>
        assert.isFunction(identity.setArtist)
      )

      test('getTracks method is defined', () =>
        assert.isFunction(identity.getTracks)
      )

      test('getTracks method expects one argument', () =>
        assert.lengthOf(identity.getTracks, 1)
      )

      test('setTrack method is defined', () =>
        assert.isFunction(identity.setTrack)
      )

      test('setTrack method expects one argument', () =>
        assert.lengthOf(identity.setTrack, 1)
      )

      test('deleteTrack method is defined', () =>
        assert.isFunction(identity.deleteTrack)
      )

      test('deleteTrack method expects one argument', () =>
        assert.lengthOf(identity.deleteTrack, 1)
      )

      suite('getArtists:', () => {
        let artists

        suiteSetup(() => artists = identity.getArtists('foo'))

        test('getArtists resolves to array', () =>
          assert.isArray(artists)
        )

        test('artists array is empty', () =>
          assert.lengthOf(artists, 0)
        )
      })

      suite('getTracks:', () => {
        let tracks

        suiteSetup(() => tracks = identity.getTracks('foo'))

        test('getTracks resolves to array', () =>
          assert.isArray(tracks)
        )

        test('tracks array is empty', () =>
          assert.lengthOf(tracks, 0)
        )
      })

      suite('setArtist:', () => {
        suiteSetup(() => identity.setArtist('foo bar'))

        suite('getArtists with mismatch:', () => {
          let artists

          suiteSetup(() => artists = identity.getArtists('foo baz'))

          test('artists array is empty', () =>
            assert.lengthOf(artists, 0)
          )
        })

        suite('getArtists with partial match (first word):', () => {
          let artists

          suiteSetup(() => artists = identity.getArtists('oo'))

          test('artists array has one item', () =>
            assert.lengthOf(artists, 1)
          )

          test('artists array item has correct name property', () =>
            assert.equal(artists[0].name, 'foo bar')
          )
        })

        suite('getArtists with partial match (second word):', () => {
          let artists

          suiteSetup(() => artists = identity.getArtists('ar'))

          test('artists array has one item', () =>
            assert.lengthOf(artists, 1)
          )

          test('artists array item has correct name property', () =>
            assert.equal(artists[0].name, 'foo bar')
          )
        })

        suite('getArtists with partial match (both words):', () => {
          let artists

          suiteSetup(() => artists = identity.getArtists('fo ba'))

          test('artists array has one item', () =>
            assert.lengthOf(artists, 1)
          )

          test('artists array item has correct name property', () =>
            assert.equal(artists[0].name, 'foo bar')
          )
        })

        suite('getArtists with match (swapped order):', () => {
          let artists

          suiteSetup(() => artists = identity.getArtists('bar foo'))

          test('artists array has one item', () =>
            assert.lengthOf(artists, 1)
          )

          test('artists array item has correct name property', () =>
            assert.equal(artists[0].name, 'foo bar')
          )
        })

        suite('getTracks with exact match:', () => {
          let tracks

          suiteSetup(() => tracks = identity.getTracks('foo bar'))

          test('tracks array is empty', () =>
            assert.lengthOf(tracks, 0)
          )
        })

        suite('getArtists with exact match:', () => {
          let artists

          suiteSetup(() => artists = identity.getArtists('foo bar'))

          test('artists array has one item', () =>
            assert.lengthOf(artists, 1)
          )

          test('artists array item is object', () =>
            assert.isObject(artists[0])
          )

          test('artists array item has two properties', () =>
            assert.lengthOf(Object.keys(artists[0]), 2)
          )

          test('artists array item has correct name property', () =>
            assert.equal(artists[0].name, 'foo bar')
          )

          test('artists array item has plausible id property', () => {
            assert.isString(artists[0].id)
            assert.match(artists[0].id, /^[0-9a-f]{64}$/)
          })
        })

        suite('setArtist:', () => {
          suiteSetup(() => identity.setArtist('foo baz'))

          suite('getArtists with partial match (first artist):', () => {
            let artists

            suiteSetup(() => artists = identity.getArtists('bar'))

            test('artists array has one item', () =>
              assert.lengthOf(artists, 1)
            )

            test('artists array item has correct name property', () =>
              assert.equal(artists[0].name, 'foo bar')
            )
          })

          suite('getArtists with partial match (second artist):', () => {
            let artists

            suiteSetup(() => artists = identity.getArtists('baz'))

            test('artists array has one item', () =>
              assert.lengthOf(artists, 1)
            )

            test('artists array item has correct name property', () =>
              assert.equal(artists[0].name, 'foo baz')
            )
          })

          suite('getArtists with partial match (both artists):', () => {
            let artists

            suiteSetup(() => artists = identity.getArtists('ba'))

            test('artists array has two items', () =>
              assert.lengthOf(artists, 2)
            )

            test('artists array has correct name property for first item', () =>
              assert.equal(artists[0].name, 'foo bar')
            )

            test('artists array has correct name property for second item', () =>
              assert.equal(artists[1].name, 'foo baz')
            )

            test('artists array has different id properties for each item', () =>
              assert.notEqual(artists[0].id, artists[1].id)
            )
          })

          suite('deleteArtist:', () => {
            suiteSetup(() => identity.deleteArtist(identity.getArtists('foo baz')[0].id))

            suite('getArtists with exact match (second artist):', () => {
              let artistsInner

              suiteSetup(() => artistsInner = identity.getArtists('foo baz'))

              test('artists array is empty', () =>
                assert.lengthOf(artistsInner, 0)
              )
            })
          })
        })

        suite('deleteArtist:', () => {
          suiteSetup(() => identity.deleteArtist(identity.getArtists('foo bar')[0].id))

          suite('getArtists with exact match:', () => {
            let artistsInner

            suiteSetup(() => artistsInner = identity.getArtists('foo bar'))

            test('artists array is empty', () =>
              assert.lengthOf(artistsInner, 0)
            )
          })
        })
      })

      suite('setTrack:', () => {
        suiteSetup(() => identity.setTrack('wibble'))

        suite('getTracks with mismatch:', () => {
          let tracks

          suiteSetup(() => tracks = identity.getTracks('wibbel'))

          test('tracks array is empty', () =>
            assert.lengthOf(tracks, 0)
          )
        })

        suite('getTracks with partial match:', () => {
          let tracks

          suiteSetup(() => tracks = identity.getTracks('wib'))

          test('tracks array has one item', () =>
            assert.lengthOf(tracks, 1)
          )

          test('tracks array item has correct name property', () =>
            assert.equal(tracks[0].name, 'wibble')
          )
        })

        suite('getArtists with exact match:', () => {
          let artists

          suiteSetup(() => artists = identity.getArtists('wibble'))

          test('artists array is empty', () =>
            assert.lengthOf(artists, 0)
          )
        })

        suite('getTracks with exact match:', () => {
          let tracks

          suiteSetup(() => tracks = identity.getTracks('wibble'))

          test('tracks array has one item', () =>
            assert.lengthOf(tracks, 1)
          )

          test('tracks array item is object', () =>
            assert.isObject(tracks[0])
          )

          test('tracks array item has two properties', () =>
            assert.lengthOf(Object.keys(tracks[0]), 2)
          )

          test('tracks array item has correct name property', () =>
            assert.equal(tracks[0].name, 'wibble')
          )

          test('tracks array item has plausible id property', () => {
            assert.isString(tracks[0].id)
            assert.match(tracks[0].id, /^[0-9a-f]{64}$/)
          })
        })

        suite('setTrack:', () => {
          suiteSetup(() => identity.setTrack('blee'))

          suite('getTracks with partial match (first track):', () => {
            let tracks

            suiteSetup(() => tracks = identity.getTracks('bbl'))

            test('tracks array has one item', () =>
              assert.lengthOf(tracks, 1)
            )

            test('tracks array item has correct name property', () =>
              assert.equal(tracks[0].name, 'wibble')
            )
          })

          suite('getTracks with partial match (second track):', () => {
            let tracks

            suiteSetup(() => tracks = identity.getTracks('lee'))

            test('tracks array has one item', () =>
              assert.lengthOf(tracks, 1)
            )

            test('tracks array item has correct name property', () =>
              assert.equal(tracks[0].name, 'blee')
            )
          })

          suite('getTracks with partial match (both tracks):', () => {
            let tracks

            suiteSetup(() => tracks = identity.getTracks('ble'))

            test('tracks array has two items', () =>
              assert.lengthOf(tracks, 2)
            )

            test('tracks array has correct name property for first item', () =>
              assert.equal(tracks[0].name, 'blee')
            )

            test('tracks array has correct name property for second item', () =>
              assert.equal(tracks[1].name, 'wibble')
            )

            test('tracks array has different id properties for each item', () =>
              assert.notEqual(tracks[0].id, tracks[1].id)
            )
          })

          suite('deleteTrack:', () => {
            suiteSetup(() => identity.deleteTrack(identity.getTracks('blee')[0].id))

            suite('getTracks with exact match (second track):', () => {
              let tracksInner

              suiteSetup(() => tracksInner = identity.getTracks('blee'))

              test('tracks array is empty', () =>
                assert.lengthOf(tracksInner, 0)
              )
            })
          })
        })

        suite('deleteTrack:', () => {
          suiteSetup(() => identity.deleteTrack(identity.getTracks('wibble')[0].id))

          suite('getTracks with exact match:', () => {
            let tracksInner

            suiteSetup(() => tracksInner = identity.getTracks('wibble'))

            test('tracks array is empty', () =>
              assert.lengthOf(tracksInner, 0)
            )
          })
        })
      })
    })
  })
})

