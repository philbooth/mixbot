// Copyright © 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert
const config = require('../../../config').get('identity.server')
const helpers = require('../../../src/test-helpers')

const modulePath = '../../../src/identity/server/routes'
const client = helpers.initialiseClient({
  baseUrl: `http://127.0.0.1:${config.port}/`,
  json: true,
  timeout: 3000
})

suite('identity/server/routes:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  suite('require:', () => {
    let routes

    suiteSetup(() => require(modulePath).then(r => routes = r))

    test('require returns array', () =>
      assert.isArray(routes)
    )

    test('array has four routes', () => {
      assert.lengthOf(routes, 4)

      helpers.assertIsRoute(routes[0])
      helpers.assertIsRoute(routes[1])
      helpers.assertIsRoute(routes[2])
      helpers.assertIsRoute(routes[3])
    })

    test('first route is correct', () => {
      assert.equal(routes[0].method, 'GET')
      assert.equal(routes[0].path, '/artists/{pattern}')

      assert.lengthOf(Object.keys(routes[0].config.validate), 1)
      helpers.assertJoiKeys(routes[0].config.validate.params, [ 'pattern' ])

      assert.isObject(routes[0].config.response)
      helpers.assertIsJoi(routes[0].config.response.schema)
    })

    test('second route is correct', () => {
      assert.equal(routes[1].method, 'PUT')
      assert.equal(routes[1].path, '/artist')

      assert.lengthOf(Object.keys(routes[1].config.validate), 1)
      helpers.assertJoiKeys(routes[1].config.validate.payload, [ 'name' ])

      assert.isUndefined(routes[1].config.response)
    })

    test('third route is correct', () => {
      assert.equal(routes[2].method, 'GET')
      assert.equal(routes[2].path, '/tracks/{pattern}')

      assert.lengthOf(Object.keys(routes[2].config.validate), 1)
      helpers.assertJoiKeys(routes[2].config.validate.params, [ 'pattern' ])

      assert.isObject(routes[2].config.response)
      helpers.assertIsJoi(routes[2].config.response.schema)
    })

    test('fourth route is correct', () => {
      assert.equal(routes[3].method, 'PUT')
      assert.equal(routes[3].path, '/track')

      assert.lengthOf(Object.keys(routes[3].config.validate), 1)
      helpers.assertJoiKeys(routes[3].config.validate.payload, [ 'name' ])

      assert.isUndefined(routes[3].config.response)
    })

    suite('start server:', () => {
      let server

      suiteSetup(() =>
        require('../../../src/identity/server')({})
          .then(s => server = s)
      )

      suiteTeardown(() => server.stop())

      suite('attempt to add an artist with an empty name:', () => {
        let result, error

        suiteSetup(() =>
          client.put({ url: '/artist', body: { name: '' } })
            .then(r => result = r)
            .catch(e => error = e)
        )

        test('result is undefined', () =>
          assert.isUndefined(result)
        )

        test('error.message is correct', () =>
          assert.equal(error.message, 'Bad Request: child "name" fails because ["name" is not allowed to be empty]')
        )

        test('error.status is correct', () =>
          assert.equal(error.status, 400)
        )

        test('error.validation is correct', () =>
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'name' ]
          })
        )
      })

      suite('attempt to add an artist with a single-character name:', () => {
        let result, error

        suiteSetup(() =>
          client.put({ url: '/artist', body: { name: 'p' } })
            .then(r => result = r)
            .catch(e => error = e)
        )

        test('result is undefined', () =>
          assert.isUndefined(result)
        )

        test('error.message is correct', () =>
          assert.equal(error.message, 'Bad Request: child "name" fails because ["name" length must be at least 2 characters long]')
        )

        test('error.status is correct', () =>
          assert.equal(error.status, 400)
        )

        test('error.validation is correct', () =>
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'name' ]
          })
        )
      })

      suite('attempt to add an artist with outer whitespace:', () => {
        let result, error

        suiteSetup(() =>
          client.put({ url: '/artist', body: { name: ' pb ' } })
            .then(r => result = r)
            .catch(e => error = e)
        )

        test('result is undefined', () =>
          assert.isUndefined(result)
        )

        test('error.message is correct', () =>
          assert.equal(error.message, 'Bad Request: child "name" fails because ["name" must not have leading or trailing whitespace]')
        )

        test('error.status is correct', () =>
          assert.equal(error.status, 400)
        )

        test('error.validation is correct', () =>
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'name' ]
          })
        )
      })

      suite('attempt to add a track with an empty name:', () => {
        let result, error

        suiteSetup(() =>
          client.put({ url: '/track', body: { name: '' } })
            .then(r => result = r)
            .catch(e => error = e)
        )

        test('result is undefined', () =>
          assert.isUndefined(result)
        )

        test('error.message is correct', () =>
          assert.equal(error.message, 'Bad Request: child "name" fails because ["name" is not allowed to be empty]')
        )

        test('error.status is correct', () =>
          assert.equal(error.status, 400)
        )

        test('error.validation is correct', () =>
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'name' ]
          })
        )
      })

      suite('attempt to add a track with a single-character name:', () => {
        let result, error

        suiteSetup(() =>
          client.put({ url: '/track', body: { name: 'p' } })
            .then(r => result = r)
            .catch(e => error = e)
        )

        test('result is undefined', () =>
          assert.isUndefined(result)
        )

        test('error.message is correct', () =>
          assert.equal(error.message, 'Bad Request: child "name" fails because ["name" length must be at least 2 characters long]')
        )

        test('error.status is correct', () =>
          assert.equal(error.status, 400)
        )

        test('error.validation is correct', () =>
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'name' ]
          })
        )
      })

      suite('attempt to add a track with outer whitespace:', () => {
        let result, error

        suiteSetup(() =>
          client.put({ url: '/track', body: { name: ' pb ' } })
            .then(r => result = r)
            .catch(e => error = e)
        )

        test('result is undefined', () =>
          assert.isUndefined(result)
        )

        test('error.message is correct', () =>
          assert.equal(error.message, 'Bad Request: child "name" fails because ["name" must not have leading or trailing whitespace]')
        )

        test('error.status is correct', () =>
          assert.equal(error.status, 400)
        )

        test('error.validation is correct', () =>
          assert.deepEqual(error.validation, {
            source: 'payload',
            keys: [ 'name' ]
          })
        )
      })

      suite('add an artist:', () => {
        let artist

        suiteSetup(() => {
          artist = 'pb'

          return client.put({
            url: '/artist',
            body: { name: artist }
          })
        })

        suite('get artists with match:', () => {
          let result

          suiteSetup(() =>
            client.get(`/artists/${artist}`)
              .then(r => result = r)
          )

          test('result is array', () =>
            assert.isArray(result)
          )

          test('result array contains one item', () =>
            assert.lengthOf(result, 1)
          )

          test('array item is object', () =>
            assert.isObject(result[0])
          )

          test('array item has 2 keys', () =>
            assert.lengthOf(Object.keys(result[0]), 2)
          )

          test('array item has plausible id property', () =>
            assert.match(result[0].id, /^[0-9a-f]{64}$/)
          )

          test('array item has correct name property', () =>
            assert.equal(result[0].name, artist)
          )
        })

        suite('get artists without match:', () => {
          let result

          suiteSetup(() =>
            client.get('/artists/bp')
              .then(r => result = r)
          )

          test('result is array', () =>
            assert.isArray(result)
          )

          test('result array is empty', () =>
            assert.lengthOf(result, 0)
          )
        })

        suite('attempt to get artists with an empty pattern:', () => {
          let result, error

          suiteSetup(() =>
            client.get({ url: '/artists/' })
              .then(r => result = r)
              .catch(e => error = e)
          )

          test('result is undefined', () =>
            assert.isUndefined(result)
          )

          test('error.status is correct', () =>
            assert.equal(error.status, 404)
          )
        })
      })

      suite('add an artist with whitespace:', () => {
        suiteSetup(() =>
          client.put({
            url: '/artist',
            body: { name: 'foo bar' }
          })
        )

        suite('get artists with match:', () => {
          let result

          suiteSetup(() =>
            client.get('/artists/oo%20ar')
              .then(r => result = r)
          )

          test('result array contains one item', () =>
            assert.lengthOf(result, 1)
          )

          test('array item has plausible id property', () =>
            assert.match(result[0].id, /^[0-9a-f]{64}$/)
          )

          test('array item has correct name property', () =>
            assert.equal(result[0].name, 'foo bar')
          )
        })
      })
    })
  })
})

