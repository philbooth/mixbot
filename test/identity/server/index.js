// Copyright © 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

/* eslint-env mocha */
/* eslint max-nested-callbacks: 0 */

'use strict'

const assert = require('chai').assert

const modulePath = '../../../src/identity/server'

suite('identity/server:', () => {
  test('require does not throw', () =>
    assert.doesNotThrow(() => require(modulePath))
  )

  test('require returns function', () =>
    assert.isFunction(require(modulePath))
  )

  suite('require:', () => {
    let start

    suiteSetup(() => start = require(modulePath))

    test('start expects one argument', () =>
      assert.lengthOf(start, 1)
    )

    test('start does not reject', () =>
      start({})
        .then(server => {
          assert.isOk(true)
          return server.stop()
        })
        .catch(() => assert.fail('reject', 'resolve'))
    )

    test('start rejects with bad arguments', () =>
      start([])
        .then(server => {
          assert.fail('resolve', 'reject')
          return server.stop()
        })
        .catch(() => assert.isOk(true))
    )

    suite('start:', () => {
      let server

      setup(() => start({}).then(s => server = s))

      teardown(() => server.stop())

      test('start returns object', () =>
        assert.isObject(server)
      )

      test('server object has one method', () =>
        assert.lengthOf(Object.keys(server), 1)
      )

      test('server object has stop method', () =>
        assert.isFunction(server.stop)
      )

      test('server.stop expects no arguments', () =>
        assert.lengthOf(server.stop, 0)
      )
    })
  })
})

