// Copyright © 2016, 2017 Phil Booth.
//
// This file is part of mixbot.
//
// mixbot is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mixbot is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mixbot. If not, see <http://www.gnu.org/licenses/>.

'use strict'

const convict = require('convict')
const path = require('path')

const config = convict({
  env: {
    format: [ 'production', 'development', '' ],
    default: '',
    env: 'NODE_ENV',
    arg: 'env'
  },
  graph: {
    database: {
      path: {
        format: String,
        default: '/var/mixbot-graph/.data',
        env: 'MIXBOT_GRAPH_DATABASE_PATH'
      }
    },
    server: {
      host: {
        format: String,
        default: '0.0.0.0',
        env: 'MIXBOT_GRAPH_SERVER_HOST'
      },
      port: {
        format: 'port',
        default: 9000,
        env: 'MIXBOT_GRAPH_SERVER_PORT'
      }
    }
  },
  identity: {
    database: {
      path: {
        format: String,
        default: '/var/mixbot-identity/.data',
        env: 'MIXBOT_IDENTITY_DATABASE_PATH'
      }
    },
    server: {
      host: {
        format: String,
        default: '0.0.0.0',
        env: 'MIXBOT_IDENTITY_SERVER_HOST'
      },
      port: {
        format: 'port',
        default: 9001,
        env: 'MIXBOT_IDENTITY_SERVER_PORT'
      }
    }
  }
})

const env = config.get('env')

if (env) {
  config.loadFile(path.resolve(__dirname, `${env}.json`))
}

module.exports = config

