# Contribution guidelines

* Install/update the dependencies:
  ```
  npm install
  ```

* Make sure the code lints:
  ```
  npm run lint
  ```

* Make sure all of the tests pass:
  ```
  npm test
  ```

* Adhere to the coding conventions
  that are used elsewhere in the codebase.

* New code must have new unit tests.

* Add yourself to the [authors] file.

* Feel free to [open an issue][newissue] first,
  if the change is one that you think
  needs some discussion.

[authors]: https://gitlab.com/philbooth/mixbot-graph/blob/master/AUTHORS
[newissue]: https://gitlab.com/philbooth/mixbot-graph/issues/new
[issues]: https://gitlab.com/philbooth/mixbot-graph/issues

